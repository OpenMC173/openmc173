#!/usr/bin/env bash

set -e

COLOR='\033[0;35m'
NC='\033[0m' # No Color


LIB_EXTENSION=''

case "$(uname)" in
    'Linux')
        LIB_EXTENSION='.so'
    ;;

    'Darwin')
        LIB_EXTENSION='.dylib'
    ;;

    *)
        echo -e "${COLOR}Unknown platform: $(uname)${NC}"
esac


if [ -e "./libraylib$LIB_EXTENSION" ]; then
    echo -e "${COLOR}libraylib$LIB_EXTENSION already exists project root. To force re-build, delete or rename libraylib$LIB_EXTENSION${NC}"
    exit 0
fi

echo -e "${COLOR}Making raylib...${NC}"

pushd ./raylib/src
make clean
make RAYLIB_LIBTYPE=SHARED RAYLIB_BUILD_MODE=DEBUG
popd

echo -e "${COLOR}Copying libraylib$LIB_EXTENSION to project root...${NC}"
cp -L "./raylib/src/libraylib$LIB_EXTENSION" .
echo -e "${COLOR}Done${NC}"

tput rmso
