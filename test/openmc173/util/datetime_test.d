module openmc173.util.datetime_test;

import exceeds_expectations;
import openmc173.util.datetime;
import std.datetime : SysTime;



@("Replace invalid path characters")
unittest
{
    import std.exception : assertThrown;

    version(Windows)
    {
        string clean1 = replaceInvalidPathChars("abc??s");
        expect(clean1).toEqual("abcs");

        string clean2 = replaceInvalidPathChars("abc??s", "_");
        expect(clean2).toEqual("abc_s");

        string clean3 = replaceInvalidPathChars("abc??s", "_", false);
        expect(clean3).toEqual("abc__s");

        string clean4 = replaceInvalidPathChars(`abc><:"/\|?*s`);
        expect(clean4).toEqual("abcs");


        assertThrown(replaceInvalidPathChars("abc", ":"), "Expected exception to be throw on trying to replace invalid path chars with \":\".");
    }

    version(Posix)
    {
        string clean1 = replaceInvalidPathChars("abc//s");
        expect(clean1).toEqual("abcs");

        string clean2 = replaceInvalidPathChars("abc//s", "_");
        expect(clean2).toEqual("abc_s");

        string clean3 = replaceInvalidPathChars("abc//s", "_", false);
        expect(clean3).toEqual("abc__s");

        string clean4 = replaceInvalidPathChars("abc><:\"/\\|?*\0\0s");
        expect(clean4).toEqual(`abc><:"\|?*s`);

        assertThrown(replaceInvalidPathChars("abc", "/"), "Expected exception to be throw on trying to replace invalid path chars with \"/\".");
    }
}

@("formatTime")
unittest
{
    import std.datetime : DateTime, dur, Duration;
    import std.datetime.timezone : SimpleTimeZone, TimeZone, UTC;
    import std.format : stringfmt = format;

    SysTime testUTC = SysTime(DateTime(2019, 11, 17, 20, 10, 35), dur!"msecs"(736), UTC());
    string expectedUTC = "2019-11-17 20:10:35.736 +00:00";
    string actualUTC = formatTime(testUTC);
    assert(actualUTC == expectedUTC, "Expected \"%s\", received \"%s\".".stringfmt(expectedUTC, actualUTC));

    immutable TimeZone est = new immutable SimpleTimeZone(dur!"hours"(-5), "EST");
    SysTime testEST = SysTime(DateTime(2019, 11, 17, 20, 10, 35), dur!"msecs"(736), est);
    string expectedEST = "2019-11-17 20:10:35.736 -05:00";
    string actualEST = formatTime(testEST);
    assert(actualEST == expectedEST, "Expected \"%s\", received \"%s\".".stringfmt(expectedEST, actualEST));

    immutable TimeZone cet = new immutable SimpleTimeZone(dur!"hours"(1), "CET");
    SysTime testCET = SysTime(DateTime(2019, 11, 17, 20, 10, 35), dur!"msecs"(736), cet);
    string expectCET = "2019-11-17 20:10:35.736 +01:00";
    string actualCET = formatTime(testCET);
    assert(actualCET == expectCET, "Expected \"%s\", received \"%s\".".stringfmt(expectCET, actualCET));

    immutable TimeZone npt = new immutable SimpleTimeZone(dur!"hours"(5) + dur!"minutes"(45), "NPT");
    SysTime testNPT = SysTime(DateTime(2019, 11, 17, 20, 10, 35), dur!"msecs"(736), npt);
    string expectNPT = "2019-11-17 20:10:35.736 +05:45";
    string actualNPT = formatTime(testNPT);
    assert(actualNPT == expectNPT, "Expected \"%s\", received \"%s\".".stringfmt(expectNPT, actualNPT));
}
