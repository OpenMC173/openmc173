module openmc173.util.graphics_test;

import bindbc.raylib.types : Vector3;
import exceeds_expectations;
import openmc173.util.graphics;
import std.conv : to;


@("Is the triangle {-3, -3, -3} closer to <5, 5, 5> than {1, 1, 1}?")
unittest
{
    bool wellIsIt = isFirstTriangleCloserThanSecond(
        [
            Vector3(0, 1, 0),
            Vector3(0, 0, 1),
            Vector3(1, 0, 0),

            Vector3(0, -3, 0),
            Vector3(0, 0, -3),
            Vector3(-3, 0, 0),
        ].ptr,
        Vector3(5, 5, 5),
        0, 1, 2, 3, 4, 5
    );

    expect(wellIsIt).toEqual(true);
}

@("Sort triangles")
unittest
{
    Vector3[] vertices = [
        Vector3(0, 1, 0),
        Vector3(0, 0, 1),
        Vector3(1, 0, 0),

        Vector3(0, 3, 0),
        Vector3(0, 0, 3),
        Vector3(3, 0, 0),

        Vector3(0, 2, 0),
        Vector3(0, 0, 2),
        Vector3(2, 0, 0),

        Vector3(0, -3, 0),
        Vector3(0, 0, -3),
        Vector3(-3, 0, 0),

        Vector3(0, -2, 0),
        Vector3(0, 0, -2),
        Vector3(-2, 0, 0),

        Vector3(0, -1, 0),
        Vector3(0, 0, -1),
        Vector3(-1, 0, 0),
    ];

    ushort[] indices = [
         0,  1,  2,
         3,  4,  5,
         6,  7,  8,
         9, 10, 11,
        12, 13, 14,
        15, 16, 17,
    ];

    sortTrianglesByDistance(
        vertices.ptr,
        indices.ptr,
        (indices.length / 3).to!int,
        Vector3(5, 5, 5)
    );

    expect(indices).toEqual([
         9, 10, 11,
        12, 13, 14,
        15, 16, 17,
         0,  1,  2,
         6,  7,  8,
         3,  4,  5,
    ]);
}
