module openmc173.datatypes.ndarray_test;

import exceeds_expectations;
import openmc173.datatypes.ndarray;
import openmc173.util.range;
import std.algorithm;


@("generateOpIndexFunction()")
unittest
{
    expect(generateOpIndexFunction(1)).toEqual(`ref E opIndex(size_t idx0) { size_t idx; size_t[N] params = [idx0]; idx += params[axisOrder[0]]; return data[idx]; }`);
    expect(generateOpIndexFunction(2)).toEqual(`ref E opIndex(size_t idx0, size_t idx1) { size_t idx; size_t[N] params = [idx0, idx1]; idx += params[axisOrder[0]] * dimensions[axisOrder[1]]; idx += params[axisOrder[1]]; return data[idx]; }`);
    expect(generateOpIndexFunction(3)).toEqual(`ref E opIndex(size_t idx0, size_t idx1, size_t idx2) { size_t idx; size_t[N] params = [idx0, idx1, idx2]; idx += params[axisOrder[0]] * dimensions[axisOrder[2]] * dimensions[axisOrder[1]]; idx += params[axisOrder[1]] * dimensions[axisOrder[2]]; idx += params[axisOrder[2]]; return data[idx]; }`);
    expect(generateOpIndexFunction(4)).toEqual(`ref E opIndex(size_t idx0, size_t idx1, size_t idx2, size_t idx3) { size_t idx; size_t[N] params = [idx0, idx1, idx2, idx3]; idx += params[axisOrder[0]] * dimensions[axisOrder[3]] * dimensions[axisOrder[2]] * dimensions[axisOrder[1]]; idx += params[axisOrder[1]] * dimensions[axisOrder[3]] * dimensions[axisOrder[2]]; idx += params[axisOrder[2]] * dimensions[axisOrder[3]]; idx += params[axisOrder[3]]; return data[idx]; }`);
    expect(generateOpIndexFunction(7)).toEqual(`ref E opIndex(size_t idx0, size_t idx1, size_t idx2, size_t idx3, size_t idx4, size_t idx5, size_t idx6) { size_t idx; size_t[N] params = [idx0, idx1, idx2, idx3, idx4, idx5, idx6]; idx += params[axisOrder[0]] * dimensions[axisOrder[6]] * dimensions[axisOrder[5]] * dimensions[axisOrder[4]] * dimensions[axisOrder[3]] * dimensions[axisOrder[2]] * dimensions[axisOrder[1]]; idx += params[axisOrder[1]] * dimensions[axisOrder[6]] * dimensions[axisOrder[5]] * dimensions[axisOrder[4]] * dimensions[axisOrder[3]] * dimensions[axisOrder[2]]; idx += params[axisOrder[2]] * dimensions[axisOrder[6]] * dimensions[axisOrder[5]] * dimensions[axisOrder[4]] * dimensions[axisOrder[3]]; idx += params[axisOrder[3]] * dimensions[axisOrder[6]] * dimensions[axisOrder[5]] * dimensions[axisOrder[4]]; idx += params[axisOrder[4]] * dimensions[axisOrder[6]] * dimensions[axisOrder[5]]; idx += params[axisOrder[5]] * dimensions[axisOrder[6]]; idx += params[axisOrder[6]]; return data[idx]; }`);
}

@("generateLoopCall()")
unittest
{
    expect(generateLoopCall("loop", "coords", 2, "element")).toEqual("loop(coords[0], coords[1], element)");
    expect(generateLoopCall("l", "i", 1, "e")).toEqual("l(i[0], e)");
    expect(generateLoopCall("l", "i", 2, "e")).toEqual("l(i[0], i[1], e)");
    expect(generateLoopCall("l", "i", 3, "e")).toEqual("l(i[0], i[1], i[2], e)");
    expect(generateLoopCall("l", "i", 4, "e")).toEqual("l(i[0], i[1], i[2], i[3], e)");
    expect(generateLoopCall("l", "i", 5, "e")).toEqual("l(i[0], i[1], i[2], i[3], i[4], e)");
}

@("generateOpApplyParamType")
unittest
{
    expect(generateOpApplyParamType(0)).toEqual("int delegate(ref E)");
    expect(generateOpApplyParamType(1)).toEqual("int delegate(size_t, ref E)");
    expect(generateOpApplyParamType(2)).toEqual("int delegate(size_t, size_t, ref E)");
    expect(generateOpApplyParamType(3)).toEqual("int delegate(size_t, size_t, size_t, ref E)");
}

@("NDarray basic")
unittest
{
    NDarray!(int, 16, 128, 16) a;
    expect(a.RawType.stringof).toEqual("int[32768]");
    expect(a.getData.length).toEqual(16 * 128 * 16);
    expect(a.getData.length).toEqual(a.CAPACITY);
}

@("NDarray.flat()")
unittest
{
    NDarray!(int, 2, 5, 11) arr;
    int[] flattened = arr.flat;
    expect(flattened.length).toEqual(2 * 5 * 11);
    expect(flattened).toBe(arr.getData);
}

/// Make sure that an NDarray with N dimensions and element type T has an
/// opIndex with N parameters, each of type T.
@("NDarray.opIndex() get")
unittest
{
    NDarray!(int, 10)()[0];
    NDarray!(int, 10)()[1];
    static assert(!__traits(compiles, NDarray!(int, 10)()[0, 0]));
    static assert(!__traits(compiles, NDarray!(int, 10)()[0, 0, 0]));
    static assert(!__traits(compiles, NDarray!(int, 10)()[3.4]));

    NDarray!(int, 10, 20)()[0, 0];
    static assert(!__traits(compiles, NDarray!(int, 10, 20)()[0]));
    static assert(!__traits(compiles, NDarray!(int, 10, 20)()[0, 0, 0]));
    static assert(!__traits(compiles, NDarray!(int, 10, 20)()[3.4, 5.5]));

    NDarray!(int, 10, 20, 30)()[0, 0, 0];
    static assert(!__traits(compiles, NDarray!(int, 10, 20, 30)()[0]));
    static assert(!__traits(compiles, NDarray!(int, 10, 20, 30)()[0, 0]));
    static assert(!__traits(compiles, NDarray!(int, 10, 20, 30)()[0, 0, 0, 0]));
    static assert(!__traits(compiles, NDarray!(int, 10, 20, 30)()[3.4, 5.5, 6.6]));

    NDarray!(int, 2, 3, 5, 7, 11)()[0, 0, 0, 0, 0];
    static assert(!__traits(compiles, NDarray!(int, 10, 20, 30, 40, 50)()[0]));
    static assert(!__traits(compiles, NDarray!(int, 10, 20, 30, 40, 50)()[0, 0]));
    static assert(!__traits(compiles, NDarray!(int, 10, 20, 30, 40, 50)()[0, 0, 0]));
    static assert(!__traits(compiles, NDarray!(int, 10, 20, 30, 40, 50)()[0, 0, 0, 0]));
    static assert(!__traits(compiles, NDarray!(int, 10, 20, 30, 40, 50)()[0, 0, 0, 0, 0, 0]));
    static assert(!__traits(compiles, NDarray!(int, 10, 20, 30, 40, 50)()[3.4, 5.5, 6.6, 7.7, 8.8]));
}

// TODO: Test this with char arrays, it didn't work earlier
@("NDarray.opIndex() set")
unittest
{
    import core.exception : RangeError;
    import std.exception : assertThrown;

    NDarray!(int, 10) x;
    x[0] = 7;
    expect(x[0]).toEqual(7);

    NDarray!(int, 5, 2) y;
    y[0, 0] = 2;
    y[4, 1] = 11;
    expect(y.getData[].count!((e) => e != int.init)).toEqual(2);

    NDarray!(int, 10, 5, 2) z;
    expect(z.flat.all!(e => e == int.init)).toEqual(true);
    z[9, 4, 1] = 13;
    expect(z[9, 4, 1]).toEqual(13);
    assertThrown!RangeError(z[10, 4, 1]);
    assertThrown!RangeError(z[9, 5, 1]);
    assertThrown!RangeError(z[9, 4, 2]);

    int i = 0;
    for (int a = 0; a < 10; a++)
    {
        for (int b = 0; b < 5; b++)
        {
            for (int c = 0; c < 2; c++)
            {
                z[a, b, c] = i;
                i++;
            }
        }
    }
    expect(z.getData).toEqual(staticIota!(int, 100));
}

@("NDarray.setData()")
unittest
{
    import std.exception : assertThrown;

    NDarray!(int, 5) a;
    a.setData([2, 3, 5, 7, 11]);
    expect(a.flat).toEqual([2, 3, 5, 7, 11]);
    a.setData([4, 8, 16, 32]);
    expect(a.flat).toEqual([4, 8, 16, 32, 11]);
    a.setData([99]);
    expect(a.flat).toEqual([99, 8, 16, 32, 11]);
}

@("NDarray axisOrder 2D")
unittest
{
    int[15] dataXY = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 ];
    int[15] dataYX = [ 1, 6, 11, 2, 7, 12, 3, 8, 13, 4, 9, 14, 5, 10, 15 ];

    NDarray!(int, 3, 5) arrXY;
    NDarray!(int, 3, 5) arrYX;
    arrXY.setData(dataXY);
    arrYX.setData(dataYX);
    arrYX.axisOrder = [1, 0];

    for (int x = 0; x < 3; x++)
    {
        for (int y = 0; y < 5; y++)
        {
            expect(arrXY[x, y]).toEqual(arrYX[x, y]);
        }
    }
}

@("NDarray axisOrder 3D")
unittest
{
    // Consider the following 3D array:
    //
    //
    //            |          11           23
    //            |        10           22
    //            |       9           21
    //            |     8           20
    //   Y axis   |           7           19
    // length = 3 |         6           18
    //            |       5           17
    //            |     4           16
    //            |           3           15       /
    //            |         2           14       /  Z axis
    //            |       1           13       /  length = 4
    //            |     0           12       /
    //
    //
    //                  --------------
    //                      X axis
    //                    length = 2
    //
    // By the default axisOrder (xyz = [0, 1, 2]), data is stored like this:
    // [ 0, 1, 2, 3, 4, 5, 6, ... ]
    //
    // But suppose we got data that looks like this:
    // [ 0, 4, 8, 1, 5, 9, 2, 6, 10, 3, 7, 11, ... ]
    // i.e., what you get when traversing the cuboid first by the y axis, then
    // the z axis, then the x axis (essentially just flipping y and z).
    //
    // You would want to treat the NDarray instance as if the data were arranged
    // in the intuitive xyz order. The solution is to set axisOrder such that
    // the NDarray instance knows what order to traverse the axes in.
    //
    // In this case, we want it to act as if the axes are arranged xzy. (x being
    // the outermost axis, y being the innermost).
    //
    // So we set axisOrder to [0, 2, 1], and then we can access elements the
    // same as we always did.
    //
    int[24] dataXYZ = staticIota!(int, 24);
    int[24] dataXZY = [ 0, 4, 8, 1, 5, 9, 2, 6, 10, 3, 7, 11, 12, 16, 20, 13, 17, 21, 14, 18, 22, 15, 19, 23 ];

    NDarray!(int, 2, 3, 4) arrXYZ;
    NDarray!(int, 2, 3, 4) arrXZY;
    arrXYZ.setData(dataXYZ);
    arrXZY.setData(dataXZY);
    arrXZY.axisOrder = [ 0, 2, 1 ];    // Like writing "x, z, y"

    for (int x = 0; x < 2; x++)
    {
        for (int y = 0; y < 3; y++)
        {
            for (int z = 0; z < 4; z++)
            {
                expect(arrXYZ[x, y, z]).toEqual(arrXZY[x, y, z]);
            }
        }
    }
}

@("NDarray axisOrder non-involutory")
unittest
{
    // Now with zxy ordering (axisOrder = [2, 0, 1]):
    // This is important and distinct from the above cases because:
    // Think of axisOrder as a bijective function that maps indices to each
    // other. For a 3D array, there are six such functions, but only two of them
    // are *not* their own inverse (Wikipedia says this is called "not
    // involutory"). There are some parts of the NDarray code which might
    // require the inverse of axisOrder, but which still pass other tests
    // because the axisOrder happens to be involutory. This test will hopefully
    // catch any such cases.
    //
    // (For a 2D array, the only two possible axisOrders are both involutory.)

    NDarray!(int, 2, 3, 4) arr;
    arr.axisOrder = [2, 0, 1];
    arr.setData(staticIota!(int, 2 * 3 * 4));

    // indexToCoords()
    expect(arr.indexToCoords(0)).toEqual([0, 0, 0]);
    expect(arr.indexToCoords(1)).toEqual([0, 1, 0]);
    expect(arr.indexToCoords(2)).toEqual([0, 2, 0]);
    expect(arr.indexToCoords(3)).toEqual([1, 0, 0]);
    expect(arr.indexToCoords(4)).toEqual([1, 1, 0]);
    expect(arr.indexToCoords(5)).toEqual([1, 2, 0]);
    expect(arr.indexToCoords(6)).toEqual([0, 0, 1]);
    expect(arr.indexToCoords(7)).toEqual([0, 1, 1]);
    expect(arr.indexToCoords(8)).toEqual([0, 2, 1]);
    expect(arr.indexToCoords(9)).toEqual([1, 0, 1]);
    expect(arr.indexToCoords(10)).toEqual([1, 1, 1]);
    expect(arr.indexToCoords(11)).toEqual([1, 2, 1]);
    expect(arr.indexToCoords(12)).toEqual([0, 0, 2]);
    expect(arr.indexToCoords(13)).toEqual([0, 1, 2]);
    expect(arr.indexToCoords(14)).toEqual([0, 2, 2]);
    expect(arr.indexToCoords(15)).toEqual([1, 0, 2]);
    expect(arr.indexToCoords(16)).toEqual([1, 1, 2]);
    expect(arr.indexToCoords(17)).toEqual([1, 2, 2]);
    expect(arr.indexToCoords(18)).toEqual([0, 0, 3]);
    expect(arr.indexToCoords(19)).toEqual([0, 1, 3]);
    expect(arr.indexToCoords(20)).toEqual([0, 2, 3]);
    expect(arr.indexToCoords(21)).toEqual([1, 0, 3]);
    expect(arr.indexToCoords(22)).toEqual([1, 1, 3]);
    expect(arr.indexToCoords(23)).toEqual([1, 2, 3]);

    // opIndex
    int[2*3*4] expectedData = staticIota!(int, 2*3*4);
    int i;
    foreach (z; 0..4)
    {
        foreach (x; 0..2)
        {
            foreach (y; 0..3)
            {
                // TODO: I'm not sure this is a good enough test of opIndex?
                expect(arr[x,y,z]).toEqual(expectedData[i]);
                i++;
            }
        }
    }
}

@("NDarray foreach")
unittest
{
    // 1D
    NDarray!(string, 10) arr1;
    string[10] data1 = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"];
    arr1.setData(["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]);
    int i;
    foreach (e; arr1)
    {
        expect(e).toEqual(data1[i]);
        i++;
    }

    // 2D
    NDarray!(string, 3, 3) arr2;
    string[9] data2 = ["a", "b", "c", "d", "e", "f", "g", "h", "i"];
    arr2.setData(data2);
    i = 0;
    foreach (e; arr2)
    {
        expect(e).toEqual(data2[i]);
        i++;
    }

    // Changing the axis order shouldn't make a difference:
    arr2.axisOrder = [1, 0];
    i = 0;
    foreach (e; arr2)
    {
        expect(e).toEqual(data2[i]);
        i++;
    }

    // 3D
    NDarray!(string, 2, 2, 2) arr3;
    string[8] data3 = ["a", "b", "c", "d", "e", "f", "g", "h"];
    arr3.setData(data3);
    i = 0;
    foreach (e; arr3)
    {
        expect(e).toEqual(data3[i]);
        i++;
    }

    // Changing the axis order shouldn't make a difference:
    arr3.axisOrder = [2, 0, 1];
    i = 0;
    foreach (e; arr3)
    {
        expect(e).toEqual(data3[i]);
        i++;
    }
}

@("NDarray foreach_reverse")
unittest
{
    NDarray!(int, 10) arr;
    arr.setData(staticIota!(int, 10));
    int i = 9;
    foreach_reverse (e; arr)
    {
        expect(e).toEqual(i);
        i--;
    }
}

@("NDarray foreach ref")
unittest
{
    // Without ref:
    NDarray!(int, 10) arr;
    arr.setData(staticIota!(int, 10));
    foreach (e; arr)
    {
        e++;
    }
    int[10] expected = staticIota!(int, 10);
    int i;
    foreach (e; arr)
    {
        expect(e).toEqual(expected[i]);
        i++;
    }

    // With ref:
    arr.setData(staticIota!(int, 10));
    foreach (ref e; arr)
    {
        e = e + 1;
    }
    expected = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    i = 0;
    foreach (e; arr)
    {
        expect(e).toEqual(expected[i]);
        i++;
    }
}

@("NDarray foreach indexed")
unittest
{
    // 1D
    NDarray!(string, 10) arr1;
    arr1.setData(["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]);
    int iManual = 0;
    foreach (iForeach, e; arr1)
    {
        expect(iManual).toEqual(iForeach);
        iManual++;
    }


    // 2D
    NDarray!(int, 4, 3) arr2;
    int[2][12] expected2 = [
        [0, 0],
        [0, 1],
        [0, 2],
        [1, 0],
        [1, 1],
        [1, 2],
        [2, 0],
        [2, 1],
        [2, 2],
        [3, 0],
        [3, 1],
        [3, 2],
    ];
    size_t[2][12] actual2;
    int i;
    foreach (x, y, e; arr2)
    {
        actual2[i] = [x, y];
        i++;
    }
    expect(actual2).toEqual(expected2);

    // Now with axisOrder = yx:
    arr2.axisOrder = [1, 0];
    expected2 = [
        [0, 0],
        [1, 0],
        [2, 0],
        [3, 0],
        [0, 1],
        [1, 1],
        [2, 1],
        [3, 1],
        [0, 2],
        [1, 2],
        [2, 2],
        [3, 2],
    ];
    i = 0;
    foreach (x, y, e; arr2)
    {
        actual2[i] = [x, y];
        i++;
    }
    expect(actual2).toEqual(expected2);

    // No further axisOrder related testing necessary, because the only part of
    // opApply() that touches axisOrder is indexToCoords(), and that has quite
    // a few axisOrder-related tests itself.


    // 3D
    NDarray!(int, 2, 3, 4) arr3;
    int[3][24] expected3 = [
        [0, 0, 0],
        [0, 0, 1],
        [0, 0, 2],
        [0, 0, 3],
        [0, 1, 0],
        [0, 1, 1],
        [0, 1, 2],
        [0, 1, 3],
        [0, 2, 0],
        [0, 2, 1],
        [0, 2, 2],
        [0, 2, 3],
        [1, 0, 0],
        [1, 0, 1],
        [1, 0, 2],
        [1, 0, 3],
        [1, 1, 0],
        [1, 1, 1],
        [1, 1, 2],
        [1, 1, 3],
        [1, 2, 0],
        [1, 2, 1],
        [1, 2, 2],
        [1, 2, 3],
    ];
    size_t[3][24] actual3;
    i = 0;
    foreach (x, y, z, e; arr3)
    {
        actual3[i] = [x, y, z];
        i++;
    }
    expect(actual3).toEqual(expected3);
}

@("NDarray foreach_reverse indexed")
unittest
{
    NDarray!(string, 10) arr;
    arr.setData(["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]);
    int iManual = 9;
    foreach_reverse (iForeach, e; arr)
    {
        expect(iManual).toEqual(iForeach);
        iManual--;
    }
}

@("NDarray indexToCoords()")
unittest
{
    // 1D
    NDarray!(int, 10) arr1;
    foreach (i; 0..10)
    {
        expect(arr1.indexToCoords(i)).toEqual([i]);
    }


    // 2D
    NDarray!(int, 3, 2) arr2;
    // First with xy ordering (axisOrder = [0,1]):
    expect(arr2.indexToCoords(0)).toEqual([0, 0]);
    expect(arr2.indexToCoords(1)).toEqual([0, 1]);
    expect(arr2.indexToCoords(2)).toEqual([1, 0]);
    expect(arr2.indexToCoords(3)).toEqual([1, 1]);
    expect(arr2.indexToCoords(4)).toEqual([2, 0]);
    expect(arr2.indexToCoords(5)).toEqual([2, 1]);

    // Now with yx ordering (axisOrder = [1, 0]):
    arr2.axisOrder = [1, 0];
    expect(arr2.indexToCoords(0)).toEqual([0, 0]);
    expect(arr2.indexToCoords(1)).toEqual([1, 0]);
    expect(arr2.indexToCoords(2)).toEqual([2, 0]);
    expect(arr2.indexToCoords(3)).toEqual([0, 1]);
    expect(arr2.indexToCoords(4)).toEqual([1, 1]);
    expect(arr2.indexToCoords(5)).toEqual([2, 1]);


    // 3D
    NDarray!(int, 2, 3, 4) arr3;
    // First with xyz ordering (axisOrder = [0,1,2]):
    expect(arr3.indexToCoords(0)).toEqual([0, 0, 0]);
    expect(arr3.indexToCoords(1)).toEqual([0, 0, 1]);
    expect(arr3.indexToCoords(2)).toEqual([0, 0, 2]);
    expect(arr3.indexToCoords(3)).toEqual([0, 0, 3]);
    expect(arr3.indexToCoords(4)).toEqual([0, 1, 0]);
    expect(arr3.indexToCoords(5)).toEqual([0, 1, 1]);
    expect(arr3.indexToCoords(6)).toEqual([0, 1, 2]);
    expect(arr3.indexToCoords(7)).toEqual([0, 1, 3]);
    expect(arr3.indexToCoords(8)).toEqual([0, 2, 0]);
    expect(arr3.indexToCoords(9)).toEqual([0, 2, 1]);
    expect(arr3.indexToCoords(10)).toEqual([0, 2, 2]);
    expect(arr3.indexToCoords(11)).toEqual([0, 2, 3]);
    expect(arr3.indexToCoords(12)).toEqual([1, 0, 0]);
    expect(arr3.indexToCoords(13)).toEqual([1, 0, 1]);
    expect(arr3.indexToCoords(14)).toEqual([1, 0, 2]);
    expect(arr3.indexToCoords(15)).toEqual([1, 0, 3]);
    expect(arr3.indexToCoords(16)).toEqual([1, 1, 0]);
    expect(arr3.indexToCoords(17)).toEqual([1, 1, 1]);
    expect(arr3.indexToCoords(18)).toEqual([1, 1, 2]);
    expect(arr3.indexToCoords(19)).toEqual([1, 1, 3]);
    expect(arr3.indexToCoords(20)).toEqual([1, 2, 0]);
    expect(arr3.indexToCoords(21)).toEqual([1, 2, 1]);
    expect(arr3.indexToCoords(22)).toEqual([1, 2, 2]);
    expect(arr3.indexToCoords(23)).toEqual([1, 2, 3]);

    // Now with yxz ordering (axisOrder = [1, 0, 2]):
    arr3.axisOrder = [1, 0, 2];
    expect(arr3.indexToCoords(0)).toEqual([0, 0, 0]);
    expect(arr3.indexToCoords(1)).toEqual([0, 0, 1]);
    expect(arr3.indexToCoords(2)).toEqual([0, 0, 2]);
    expect(arr3.indexToCoords(3)).toEqual([0, 0, 3]);
    expect(arr3.indexToCoords(4)).toEqual([1, 0, 0]);
    expect(arr3.indexToCoords(5)).toEqual([1, 0, 1]);
    expect(arr3.indexToCoords(6)).toEqual([1, 0, 2]);
    expect(arr3.indexToCoords(7)).toEqual([1, 0, 3]);
    expect(arr3.indexToCoords(8)).toEqual([0, 1, 0]);
    expect(arr3.indexToCoords(9)).toEqual([0, 1, 1]);
    expect(arr3.indexToCoords(10)).toEqual([0, 1, 2]);
    expect(arr3.indexToCoords(11)).toEqual([0, 1, 3]);
    expect(arr3.indexToCoords(12)).toEqual([1, 1, 0]);
    expect(arr3.indexToCoords(13)).toEqual([1, 1, 1]);
    expect(arr3.indexToCoords(14)).toEqual([1, 1, 2]);
    expect(arr3.indexToCoords(15)).toEqual([1, 1, 3]);
    expect(arr3.indexToCoords(16)).toEqual([0, 2, 0]);
    expect(arr3.indexToCoords(17)).toEqual([0, 2, 1]);
    expect(arr3.indexToCoords(18)).toEqual([0, 2, 2]);
    expect(arr3.indexToCoords(19)).toEqual([0, 2, 3]);
    expect(arr3.indexToCoords(20)).toEqual([1, 2, 0]);
    expect(arr3.indexToCoords(21)).toEqual([1, 2, 1]);
    expect(arr3.indexToCoords(22)).toEqual([1, 2, 2]);
    expect(arr3.indexToCoords(23)).toEqual([1, 2, 3]);
}

@("NDarray opDollar()")
unittest
{
    // 1D
    NDarray!(int, 5) arr1;
    arr1.setData(staticIota!(int, 5));
    expect(arr1[$ - 1]).toEqual(arr1[4]);


    // 2D
    NDarray!(int, 2, 3) arr2;
    arr2.setData(staticIota!(int, 6));
    expect(arr2[0, $ - 1]).toEqual(arr2[0, 2]);
    expect(arr2[$ - 1, 0]).toEqual(arr2[1, 0]);
    expect(arr2[$ - 1, $ - 1]).toEqual(arr2[1, 2]);


    // 3D (non-involutory)
    NDarray!(int, 2, 3, 4) arr3;
    arr3.axisOrder = [1, 2, 0];
    arr3.setData(staticIota!(int, 24));
    expect(arr3[0, 0, $ - 1]).toEqual(arr3[0, 0, 3]);
    expect(arr3[0, $ - 1, 0]).toEqual(arr3[0, 2, 0]);
    expect(arr3[0, $ - 1, $ - 1]).toEqual(arr3[0, 2, 3]);
    expect(arr3[$ - 1, 0, 0]).toEqual(arr3[1, 0, 0]);
    expect(arr3[$ - 1, 0, $ - 1]).toEqual(arr3[1, 0, 3]);
    expect(arr3[$ - 1, $ - 1, 0]).toEqual(arr3[1, 2, 0]);
    expect(arr3[$ - 1, $ - 1, $ - 1]).toEqual(arr3[1, 2, 3]);
}
