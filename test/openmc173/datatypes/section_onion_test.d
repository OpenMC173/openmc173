module openmc173.datatypes.section_onion_test;

import enumap;
import exceeds_expectations;
import openmc173.datatypes.block;
import openmc173.datatypes.directions;
import openmc173.datatypes.ndarray;
import openmc173.datatypes.section;
import openmc173.datatypes.section_onion;
import openmc173.datatypes.vector3i;


private static NDarray!(Block, 16, 16, 16) emptySection = NDarray!(Block, 16, 16, 16)();
private static SectionOnion.SectionLoader dummySectionLoader = (Vector3i globalCoords) => Section(globalCoords, emptySection, Enumap!(CardinalDir, Section*)());
private static SectionOnion.SectionSaver dummySectionSaver = (Section* section) {};


@("containsGlobalCoords")
unittest
{
    SectionOnion onion = new SectionOnion(
        2,
        Vector3i(1, 2, 3),
        dummySectionLoader,
        dummySectionSaver,
    );

    foreach (int x; -1 .. 2)
        foreach (int y; -1 .. 2)
            foreach (int z; -1 .. 2)
            {
                expect(Vector3i(x, y, z).add(Vector3i(1, 2, 3))).toSatisfy(pos => onion.containsGlobalCoords(pos));
            }

    foreach (int x; -20 .. -1)
        foreach (int y; -20 .. -1)
            foreach (int z; -20 .. -1)
            {
                expect(Vector3i(x, y, z).add(Vector3i(1, 2, 3))).not.toSatisfy(pos => onion.containsGlobalCoords(pos));
            }

    foreach (int x; 2 .. 20)
        foreach (int y; 2 .. 20)
            foreach (int z; 2 .. 20)
            {
                expect(Vector3i(x, y, z).add(Vector3i(1, 2, 3))).not.toSatisfy(pos => onion.containsGlobalCoords(pos));
            }
}

@("getSectionAtOnionCoords, 0 layers")
unittest
{
    import core.exception : AssertError;

    SectionOnion onion = new SectionOnion(0, Vector3i(0, 0, 0), dummySectionLoader, dummySectionSaver);
    expect(onion.getSectionAtOnionCoords(Vector3i(0, 0, 0))).toBe(null);
}

@("getSectionAtOnionCoords, 1 layer")
unittest
{
    import core.exception : AssertError;

    SectionOnion onion = new SectionOnion(1, Vector3i(0, 0, 0), dummySectionLoader, dummySectionSaver);
    expect(onion.getSectionAtOnionCoords(Vector3i(0, 0, 0))).toBe(onion.getLayers[0][0]);

    expect(onion.getSectionAtOnionCoords(Vector3i(1,  0,  0))).toBe(null);
    expect(onion.getSectionAtOnionCoords(Vector3i(-1, 0,  0))).toBe(null);
    expect(onion.getSectionAtOnionCoords(Vector3i(0,  1,  0))).toBe(null);
    expect(onion.getSectionAtOnionCoords(Vector3i(0, -1,  0))).toBe(null);
    expect(onion.getSectionAtOnionCoords(Vector3i(0,  0,  1))).toBe(null);
    expect(onion.getSectionAtOnionCoords(Vector3i(0,  0, -1))).toBe(null);
}

@("getSectionAtOnionCoords, 2 layers")
unittest
{
    import core.exception : AssertError;

    SectionOnion onion = new SectionOnion(2, Vector3i(1, 2, 3), dummySectionLoader, dummySectionSaver);

    expect(onion.getSectionAtOnionCoords(Vector3i(-1,  1, -1))).toBe(onion.getLayers[1][17]);
    expect(onion.getSectionAtOnionCoords(Vector3i( 0,  1, -1))).toBe(onion.getLayers[1][18]);
    expect(onion.getSectionAtOnionCoords(Vector3i( 1,  1, -1))).toBe(onion.getLayers[1][19]);
    expect(onion.getSectionAtOnionCoords(Vector3i(-1,  1,  0))).toBe(onion.getLayers[1][20]);
    expect(onion.getSectionAtOnionCoords(Vector3i( 0,  1,  0))).toBe(onion.getLayers[1][21]);
    expect(onion.getSectionAtOnionCoords(Vector3i( 1,  1,  0))).toBe(onion.getLayers[1][22]);
    expect(onion.getSectionAtOnionCoords(Vector3i(-1,  1,  1))).toBe(onion.getLayers[1][23]);
    expect(onion.getSectionAtOnionCoords(Vector3i( 0,  1,  1))).toBe(onion.getLayers[1][24]);
    expect(onion.getSectionAtOnionCoords(Vector3i( 1,  1,  1))).toBe(onion.getLayers[1][25]);

    expect(onion.getSectionAtOnionCoords(Vector3i(-1,  0, -1))).toBe(onion.getLayers[1][9]);
    expect(onion.getSectionAtOnionCoords(Vector3i( 0,  0, -1))).toBe(onion.getLayers[1][10]);
    expect(onion.getSectionAtOnionCoords(Vector3i( 1,  0, -1))).toBe(onion.getLayers[1][11]);
    expect(onion.getSectionAtOnionCoords(Vector3i(-1,  0,  0))).toBe(onion.getLayers[1][12]);
    expect(onion.getSectionAtOnionCoords(Vector3i( 0,  0,  0))).toBe(onion.getLayers[0][0]);
    expect(onion.getSectionAtOnionCoords(Vector3i( 1,  0,  0))).toBe(onion.getLayers[1][13]);
    expect(onion.getSectionAtOnionCoords(Vector3i(-1,  0,  1))).toBe(onion.getLayers[1][14]);
    expect(onion.getSectionAtOnionCoords(Vector3i( 0,  0,  1))).toBe(onion.getLayers[1][15]);
    expect(onion.getSectionAtOnionCoords(Vector3i( 1,  0,  1))).toBe(onion.getLayers[1][16]);

    expect(onion.getSectionAtOnionCoords(Vector3i(-1, -1, -1))).toBe(onion.getLayers[1][0]);
    expect(onion.getSectionAtOnionCoords(Vector3i( 0, -1, -1))).toBe(onion.getLayers[1][1]);
    expect(onion.getSectionAtOnionCoords(Vector3i( 1, -1, -1))).toBe(onion.getLayers[1][2]);
    expect(onion.getSectionAtOnionCoords(Vector3i(-1, -1,  0))).toBe(onion.getLayers[1][3]);
    expect(onion.getSectionAtOnionCoords(Vector3i( 0, -1,  0))).toBe(onion.getLayers[1][4]);
    expect(onion.getSectionAtOnionCoords(Vector3i( 1, -1,  0))).toBe(onion.getLayers[1][5]);
    expect(onion.getSectionAtOnionCoords(Vector3i(-1, -1,  1))).toBe(onion.getLayers[1][6]);
    expect(onion.getSectionAtOnionCoords(Vector3i( 0, -1,  1))).toBe(onion.getLayers[1][7]);
    expect(onion.getSectionAtOnionCoords(Vector3i( 1, -1,  1))).toBe(onion.getLayers[1][8]);

    expect(onion.getSectionAtOnionCoords(Vector3i(-2,  0,  0))).toBe(null);
    expect(onion.getSectionAtOnionCoords(Vector3i( 0, -2,  0))).toBe(null);
    expect(onion.getSectionAtOnionCoords(Vector3i( 0,  0, -2))).toBe(null);
    expect(onion.getSectionAtOnionCoords(Vector3i( 2,  0,  0))).toBe(null);
    expect(onion.getSectionAtOnionCoords(Vector3i( 0,  2,  0))).toBe(null);
    expect(onion.getSectionAtOnionCoords(Vector3i( 0,  0,  2))).toBe(null);
}

@("onionLayerArrayLength")
unittest
{
    expect(onionLayerArrayLength(0)).toEqual(1);
    expect(onionLayerArrayLength(1)).toEqual(3^^3 - 1);
    expect(onionLayerArrayLength(2)).toEqual(5^^3 - 3^^3);
}

@("numberOfSectionsInOnion")
unittest
{
    expect(numberOfSectionsInOnion(0)).toEqual(0);
    expect(numberOfSectionsInOnion(1)).toEqual(1);
    expect(numberOfSectionsInOnion(2)).toEqual(27);
    expect(numberOfSectionsInOnion(3)).toEqual(125);
}



@("sectionIndexToLayerCoords")
unittest
{
    expect(sectionIndexToLayerCoords(0, 0)).toEqual(Vector3i(0, 0, 0));
    // expect(sectionIndexToLayerCoords(1, 345)).toEqual(Vector3i(0, 0, 0)); // TODO: Do we care?

    // Bottom level
    expect(sectionIndexToLayerCoords(1, 0)).toEqual(Vector3i(0, 0, 0));
    expect(sectionIndexToLayerCoords(1, 1)).toEqual(Vector3i(1, 0, 0));
    expect(sectionIndexToLayerCoords(1, 2)).toEqual(Vector3i(2, 0, 0));

    expect(sectionIndexToLayerCoords(1, 3)).toEqual(Vector3i(0, 0, 1));
    expect(sectionIndexToLayerCoords(1, 4)).toEqual(Vector3i(1, 0, 1));
    expect(sectionIndexToLayerCoords(1, 5)).toEqual(Vector3i(2, 0, 1));

    expect(sectionIndexToLayerCoords(1, 6)).toEqual(Vector3i(0, 0, 2));
    expect(sectionIndexToLayerCoords(1, 7)).toEqual(Vector3i(1, 0, 2));
    expect(sectionIndexToLayerCoords(1, 8)).toEqual(Vector3i(2, 0, 2));

    // Middle level
    expect(sectionIndexToLayerCoords(1, 9)).toEqual(Vector3i(0, 1, 0));
    expect(sectionIndexToLayerCoords(1, 10)).toEqual(Vector3i(1, 1, 0));
    expect(sectionIndexToLayerCoords(1, 11)).toEqual(Vector3i(2, 1, 0));

    expect(sectionIndexToLayerCoords(1, 12)).toEqual(Vector3i(0, 1, 1));
    expect(sectionIndexToLayerCoords(1, 13)).toEqual(Vector3i(2, 1, 1));

    expect(sectionIndexToLayerCoords(1, 14)).toEqual(Vector3i(0, 1, 2));
    expect(sectionIndexToLayerCoords(1, 15)).toEqual(Vector3i(1, 1, 2));
    expect(sectionIndexToLayerCoords(1, 16)).toEqual(Vector3i(2, 1, 2));

    // Top level
    expect(sectionIndexToLayerCoords(1, 17)).toEqual(Vector3i(0, 2, 0));
    expect(sectionIndexToLayerCoords(1, 18)).toEqual(Vector3i(1, 2, 0));
    expect(sectionIndexToLayerCoords(1, 19)).toEqual(Vector3i(2, 2, 0));

    expect(sectionIndexToLayerCoords(1, 20)).toEqual(Vector3i(0, 2, 1));
    expect(sectionIndexToLayerCoords(1, 21)).toEqual(Vector3i(1, 2, 1));
    expect(sectionIndexToLayerCoords(1, 22)).toEqual(Vector3i(2, 2, 1));

    expect(sectionIndexToLayerCoords(1, 23)).toEqual(Vector3i(0, 2, 2));
    expect(sectionIndexToLayerCoords(1, 24)).toEqual(Vector3i(1, 2, 2));
    expect(sectionIndexToLayerCoords(1, 25)).toEqual(Vector3i(2, 2, 2));
}

@("layerCoordsToSectionIndex")
unittest
{
    expect(layerCoordsToSectionIndex(0, Vector3i(0, 0, 0))).toEqual(0);
    // expect(layerCoordsToSectionIndex(0, 345)).toEqual(Vector3i(0, 0, 0)); // TODO: Do we care?

    // Bottom level
    expect(layerCoordsToSectionIndex(1, Vector3i(0, 0, 0))).toEqual(0);
    expect(layerCoordsToSectionIndex(1, Vector3i(1, 0, 0))).toEqual(1);
    expect(layerCoordsToSectionIndex(1, Vector3i(2, 0, 0))).toEqual(2);

    expect(layerCoordsToSectionIndex(1, Vector3i(0, 0, 1))).toEqual(3);
    expect(layerCoordsToSectionIndex(1, Vector3i(1, 0, 1))).toEqual(4);
    expect(layerCoordsToSectionIndex(1, Vector3i(2, 0, 1))).toEqual(5);

    expect(layerCoordsToSectionIndex(1, Vector3i(0, 0, 2))).toEqual(6);
    expect(layerCoordsToSectionIndex(1, Vector3i(1, 0, 2))).toEqual(7);
    expect(layerCoordsToSectionIndex(1, Vector3i(2, 0, 2))).toEqual(8);

    // Middle level
    expect(layerCoordsToSectionIndex(1, Vector3i(0, 1, 0))).toEqual(9);
    expect(layerCoordsToSectionIndex(1, Vector3i(1, 1, 0))).toEqual(10);
    expect(layerCoordsToSectionIndex(1, Vector3i(2, 1, 0))).toEqual(11);

    expect(layerCoordsToSectionIndex(1, Vector3i(0, 1, 1))).toEqual(12);
    expect(layerCoordsToSectionIndex(1, Vector3i(2, 1, 1))).toEqual(13);

    expect(layerCoordsToSectionIndex(1, Vector3i(0, 1, 2))).toEqual(14);
    expect(layerCoordsToSectionIndex(1, Vector3i(1, 1, 2))).toEqual(15);
    expect(layerCoordsToSectionIndex(1, Vector3i(2, 1, 2))).toEqual(16);

    // Top level
    expect(layerCoordsToSectionIndex(1, Vector3i(0, 2, 0))).toEqual(17);
    expect(layerCoordsToSectionIndex(1, Vector3i(1, 2, 0))).toEqual(18);
    expect(layerCoordsToSectionIndex(1, Vector3i(2, 2, 0))).toEqual(19);

    expect(layerCoordsToSectionIndex(1, Vector3i(0, 2, 1))).toEqual(20);
    expect(layerCoordsToSectionIndex(1, Vector3i(1, 2, 1))).toEqual(21);
    expect(layerCoordsToSectionIndex(1, Vector3i(2, 2, 1))).toEqual(22);

    expect(layerCoordsToSectionIndex(1, Vector3i(0, 2, 2))).toEqual(23);
    expect(layerCoordsToSectionIndex(1, Vector3i(1, 2, 2))).toEqual(24);
    expect(layerCoordsToSectionIndex(1, Vector3i(2, 2, 2))).toEqual(25);
}

@("onionCoordsToLayerIndex")
unittest
{
    expect(onionCoordsToLayerIndex(Vector3i(0, 0, 0))).toEqual(0);

    expect(onionCoordsToLayerIndex(Vector3i(-1, -1, -1))).toEqual(1);
    expect(onionCoordsToLayerIndex(Vector3i(-1, -1,  0))).toEqual(1);
    expect(onionCoordsToLayerIndex(Vector3i(-1, -1,  1))).toEqual(1);
    expect(onionCoordsToLayerIndex(Vector3i( 0, -1, -1))).toEqual(1);
    expect(onionCoordsToLayerIndex(Vector3i( 0, -1,  0))).toEqual(1);
    expect(onionCoordsToLayerIndex(Vector3i( 0, -1,  1))).toEqual(1);
    expect(onionCoordsToLayerIndex(Vector3i( 1, -1, -1))).toEqual(1);
    expect(onionCoordsToLayerIndex(Vector3i( 1, -1,  0))).toEqual(1);
    expect(onionCoordsToLayerIndex(Vector3i( 1, -1,  1))).toEqual(1);
    expect(onionCoordsToLayerIndex(Vector3i( 0,  0, -1))).toEqual(1);
    expect(onionCoordsToLayerIndex(Vector3i( 1,  0,  0))).toEqual(1);
    expect(onionCoordsToLayerIndex(Vector3i( 0,  1,  0))).toEqual(1);
    expect(onionCoordsToLayerIndex(Vector3i( 0,  0,  1))).toEqual(1);

    expect(onionCoordsToLayerIndex(Vector3i( -3, 7, 2))).toEqual(7);
}


@("onionCoordsToLayerCoords")
unittest
{
    expect(onionCoordsToLayerCoords(Vector3i(-1, -1, -1))).toEqual(Vector3i(0, 0, 0));
    expect(onionCoordsToLayerCoords(Vector3i( 0, -1, -1))).toEqual(Vector3i(1, 0, 0));
    expect(onionCoordsToLayerCoords(Vector3i( 1, -1, -1))).toEqual(Vector3i(2, 0, 0));
    expect(onionCoordsToLayerCoords(Vector3i(-1, -1,  0))).toEqual(Vector3i(0, 0, 1));
    expect(onionCoordsToLayerCoords(Vector3i( 0, -1,  0))).toEqual(Vector3i(1, 0, 1));
    expect(onionCoordsToLayerCoords(Vector3i( 1, -1,  0))).toEqual(Vector3i(2, 0, 1));
    expect(onionCoordsToLayerCoords(Vector3i(-1, -1,  1))).toEqual(Vector3i(0, 0, 2));
    expect(onionCoordsToLayerCoords(Vector3i( 0, -1,  1))).toEqual(Vector3i(1, 0, 2));
    expect(onionCoordsToLayerCoords(Vector3i( 1, -1,  1))).toEqual(Vector3i(2, 0, 2));

    expect(onionCoordsToLayerCoords(Vector3i(-1,  0, -1))).toEqual(Vector3i(0, 1, 0));
    expect(onionCoordsToLayerCoords(Vector3i( 0,  0, -1))).toEqual(Vector3i(1, 1, 0));
    expect(onionCoordsToLayerCoords(Vector3i( 1,  0, -1))).toEqual(Vector3i(2, 1, 0));
    expect(onionCoordsToLayerCoords(Vector3i(-1,  0,  0))).toEqual(Vector3i(0, 1, 1));

    expect(onionCoordsToLayerCoords(Vector3i( 0,  0,  0))).toEqual(Vector3i(0, 0, 0));

    expect(onionCoordsToLayerCoords(Vector3i( 1,  0,  0))).toEqual(Vector3i(2, 1, 1));
    expect(onionCoordsToLayerCoords(Vector3i(-1,  0,  1))).toEqual(Vector3i(0, 1, 2));
    expect(onionCoordsToLayerCoords(Vector3i( 0,  0,  1))).toEqual(Vector3i(1, 1, 2));
    expect(onionCoordsToLayerCoords(Vector3i( 1,  0,  1))).toEqual(Vector3i(2, 1, 2));

    expect(onionCoordsToLayerCoords(Vector3i(-1,  1, -1))).toEqual(Vector3i(0, 2, 0));
    expect(onionCoordsToLayerCoords(Vector3i( 0,  1, -1))).toEqual(Vector3i(1, 2, 0));
    expect(onionCoordsToLayerCoords(Vector3i( 1,  1, -1))).toEqual(Vector3i(2, 2, 0));
    expect(onionCoordsToLayerCoords(Vector3i(-1,  1,  0))).toEqual(Vector3i(0, 2, 1));
    expect(onionCoordsToLayerCoords(Vector3i( 0,  1,  0))).toEqual(Vector3i(1, 2, 1));
    expect(onionCoordsToLayerCoords(Vector3i( 1,  1,  0))).toEqual(Vector3i(2, 2, 1));
    expect(onionCoordsToLayerCoords(Vector3i(-1,  1,  1))).toEqual(Vector3i(0, 2, 2));
    expect(onionCoordsToLayerCoords(Vector3i( 0,  1,  1))).toEqual(Vector3i(1, 2, 2));
    expect(onionCoordsToLayerCoords(Vector3i( 1,  1,  1))).toEqual(Vector3i(2, 2, 2));
}

@("layerCoordsToOnionCoords")
unittest
{
    expect(layerCoordsToOnionCoords(1, Vector3i(0, 0, 0))).toEqual(Vector3i(-1, -1, -1));
    expect(layerCoordsToOnionCoords(1, Vector3i(1, 0, 0))).toEqual(Vector3i( 0, -1, -1));
    expect(layerCoordsToOnionCoords(1, Vector3i(2, 0, 0))).toEqual(Vector3i( 1, -1, -1));
    expect(layerCoordsToOnionCoords(1, Vector3i(0, 0, 1))).toEqual(Vector3i(-1, -1,  0));
    expect(layerCoordsToOnionCoords(1, Vector3i(1, 0, 1))).toEqual(Vector3i( 0, -1,  0));
    expect(layerCoordsToOnionCoords(1, Vector3i(2, 0, 1))).toEqual(Vector3i( 1, -1,  0));
    expect(layerCoordsToOnionCoords(1, Vector3i(0, 0, 2))).toEqual(Vector3i(-1, -1,  1));
    expect(layerCoordsToOnionCoords(1, Vector3i(1, 0, 2))).toEqual(Vector3i( 0, -1,  1));
    expect(layerCoordsToOnionCoords(1, Vector3i(2, 0, 2))).toEqual(Vector3i( 1, -1,  1));

    expect(layerCoordsToOnionCoords(1, Vector3i(0, 1, 0))).toEqual(Vector3i(-1,  0, -1));
    expect(layerCoordsToOnionCoords(1, Vector3i(1, 1, 0))).toEqual(Vector3i( 0,  0, -1));
    expect(layerCoordsToOnionCoords(1, Vector3i(2, 1, 0))).toEqual(Vector3i( 1,  0, -1));
    expect(layerCoordsToOnionCoords(1, Vector3i(0, 1, 1))).toEqual(Vector3i(-1,  0,  0));

    expect(layerCoordsToOnionCoords(0, Vector3i(0, 0, 0))).toEqual(Vector3i( 0,  0,  0));

    expect(layerCoordsToOnionCoords(1, Vector3i(2, 1, 1))).toEqual(Vector3i( 1,  0,  0));
    expect(layerCoordsToOnionCoords(1, Vector3i(0, 1, 2))).toEqual(Vector3i(-1,  0,  1));
    expect(layerCoordsToOnionCoords(1, Vector3i(1, 1, 2))).toEqual(Vector3i( 0,  0,  1));
    expect(layerCoordsToOnionCoords(1, Vector3i(2, 1, 2))).toEqual(Vector3i( 1,  0,  1));

    expect(layerCoordsToOnionCoords(1, Vector3i(0, 2, 0))).toEqual(Vector3i(-1,  1, -1));
    expect(layerCoordsToOnionCoords(1, Vector3i(1, 2, 0))).toEqual(Vector3i( 0,  1, -1));
    expect(layerCoordsToOnionCoords(1, Vector3i(2, 2, 0))).toEqual(Vector3i( 1,  1, -1));
    expect(layerCoordsToOnionCoords(1, Vector3i(0, 2, 1))).toEqual(Vector3i(-1,  1,  0));
    expect(layerCoordsToOnionCoords(1, Vector3i(1, 2, 1))).toEqual(Vector3i( 0,  1,  0));
    expect(layerCoordsToOnionCoords(1, Vector3i(2, 2, 1))).toEqual(Vector3i( 1,  1,  0));
    expect(layerCoordsToOnionCoords(1, Vector3i(0, 2, 2))).toEqual(Vector3i(-1,  1,  1));
    expect(layerCoordsToOnionCoords(1, Vector3i(1, 2, 2))).toEqual(Vector3i( 0,  1,  1));
    expect(layerCoordsToOnionCoords(1, Vector3i(2, 2, 2))).toEqual(Vector3i( 1,  1,  1));
}

@("listAllGlobalCoordsInOnion")
unittest
{
    expect(listAllGlobalCoordsInOnion(Vector3i(0, 0, 0), 0).length).toEqual(0);
    expect(listAllGlobalCoordsInOnion(Vector3i(0, 0, 0), 1)).toEqual([Vector3i(0, 0, 0)]);
    expect(listAllGlobalCoordsInOnion(Vector3i(0, 0, 0), 2)).toEqual([
        Vector3i(-1, -1, -1),
        Vector3i( 0, -1, -1),
        Vector3i( 1, -1, -1),
        Vector3i(-1, -1,  0),
        Vector3i( 0, -1,  0),
        Vector3i( 1, -1,  0),
        Vector3i(-1, -1,  1),
        Vector3i( 0, -1,  1),
        Vector3i( 1, -1,  1),
        Vector3i(-1,  0, -1),
        Vector3i( 0,  0, -1),
        Vector3i( 1,  0, -1),
        Vector3i(-1,  0,  0),
        Vector3i( 0,  0,  0),
        Vector3i( 1,  0,  0),
        Vector3i(-1,  0,  1),
        Vector3i( 0,  0,  1),
        Vector3i( 1,  0,  1),
        Vector3i(-1,  1, -1),
        Vector3i( 0,  1, -1),
        Vector3i( 1,  1, -1),
        Vector3i(-1,  1,  0),
        Vector3i( 0,  1,  0),
        Vector3i( 1,  1,  0),
        Vector3i(-1,  1,  1),
        Vector3i( 0,  1,  1),
        Vector3i( 1,  1,  1),
    ]);
    expect(listAllGlobalCoordsInOnion(Vector3i(1, 2, 3), 2)).toEqual([
        Vector3i(-1, -1, -1).add(Vector3i(1, 2, 3)),
        Vector3i( 0, -1, -1).add(Vector3i(1, 2, 3)),
        Vector3i( 1, -1, -1).add(Vector3i(1, 2, 3)),
        Vector3i(-1, -1,  0).add(Vector3i(1, 2, 3)),
        Vector3i( 0, -1,  0).add(Vector3i(1, 2, 3)),
        Vector3i( 1, -1,  0).add(Vector3i(1, 2, 3)),
        Vector3i(-1, -1,  1).add(Vector3i(1, 2, 3)),
        Vector3i( 0, -1,  1).add(Vector3i(1, 2, 3)),
        Vector3i( 1, -1,  1).add(Vector3i(1, 2, 3)),
        Vector3i(-1,  0, -1).add(Vector3i(1, 2, 3)),
        Vector3i( 0,  0, -1).add(Vector3i(1, 2, 3)),
        Vector3i( 1,  0, -1).add(Vector3i(1, 2, 3)),
        Vector3i(-1,  0,  0).add(Vector3i(1, 2, 3)),
        Vector3i( 0,  0,  0).add(Vector3i(1, 2, 3)),
        Vector3i( 1,  0,  0).add(Vector3i(1, 2, 3)),
        Vector3i(-1,  0,  1).add(Vector3i(1, 2, 3)),
        Vector3i( 0,  0,  1).add(Vector3i(1, 2, 3)),
        Vector3i( 1,  0,  1).add(Vector3i(1, 2, 3)),
        Vector3i(-1,  1, -1).add(Vector3i(1, 2, 3)),
        Vector3i( 0,  1, -1).add(Vector3i(1, 2, 3)),
        Vector3i( 1,  1, -1).add(Vector3i(1, 2, 3)),
        Vector3i(-1,  1,  0).add(Vector3i(1, 2, 3)),
        Vector3i( 0,  1,  0).add(Vector3i(1, 2, 3)),
        Vector3i( 1,  1,  0).add(Vector3i(1, 2, 3)),
        Vector3i(-1,  1,  1).add(Vector3i(1, 2, 3)),
        Vector3i( 0,  1,  1).add(Vector3i(1, 2, 3)),
        Vector3i( 1,  1,  1).add(Vector3i(1, 2, 3)),
    ]);
}
