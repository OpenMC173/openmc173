module openmc173.io.textures;

import bindbc.raylib;
import enumap;
import openmc173.datatypes.block;
import openmc173.datatypes.directions;
import std.file;
import std.path;
import std.range : repeat;
import std.string;


class Textures
{
    Texture2D terrain;
    Texture2D gui;

    Enumap!(CardinalDir, Rectangle)[] terrainUVs;
    Rectangle[string] guiUVs;

    this(string texturePackName)
    {
        string texturePackDir = thisExePath().dirName().buildPath("texturepacks", texturePackName);
        if (!exists(texturePackDir) || !isDir(texturePackDir))
        {
            throw new Exception("Path is not a directory: " ~ texturePackDir);
        }

        // Terrain

        string terrainPath = buildPath(texturePackDir, "terrain.png");
        terrain = LoadTexture(terrainPath.toStringz());

        terrainUVs.length = VanillaBlockIDs.max;

        terrainUVs[VanillaBlockIDs.STONE]       = Rectangle( 1/16f,  0/16f, 1/16f, 1/16f).repeat(CardinalDir.max + 1);

        terrainUVs[VanillaBlockIDs.GRASS]       = Rectangle( 3/16f,  0/16f, 1/16f, 1/16f).repeat(CardinalDir.max + 1);
        terrainUVs[VanillaBlockIDs.GRASS].yPos  = Rectangle( 0/16f,  0/16f, 1/16f, 1/16f);
        terrainUVs[VanillaBlockIDs.GRASS].yNeg  = Rectangle( 2/16f,  0/16f, 1/16f, 1/16f);

        terrainUVs[VanillaBlockIDs.DIRT]        = Rectangle( 2/16f,  0/16f, 1/16f, 1/16f).repeat(CardinalDir.max + 1);
        terrainUVs[VanillaBlockIDs.COBBLE]      = Rectangle( 0/16f,  1/16f, 1/16f, 1/16f).repeat(CardinalDir.max + 1);
        terrainUVs[VanillaBlockIDs.PLANKS]      = Rectangle( 4/16f,  0/16f, 1/16f, 1/16f).repeat(CardinalDir.max + 1);
        terrainUVs[VanillaBlockIDs.BEDROCK]     = Rectangle( 1/16f,  1/16f, 1/16f, 1/16f).repeat(CardinalDir.max + 1);
        terrainUVs[VanillaBlockIDs.SAND]        = Rectangle( 2/16f,  1/16f, 1/16f, 1/16f).repeat(CardinalDir.max + 1);
        terrainUVs[VanillaBlockIDs.GRAVEL]      = Rectangle( 3/16f,  1/16f, 1/16f, 1/16f).repeat(CardinalDir.max + 1);
        terrainUVs[VanillaBlockIDs.ORE_GOLD]    = Rectangle( 0/16f,  2/16f, 1/16f, 1/16f).repeat(CardinalDir.max + 1);
        terrainUVs[VanillaBlockIDs.ORE_IRON]    = Rectangle( 1/16f,  2/16f, 1/16f, 1/16f).repeat(CardinalDir.max + 1);
        terrainUVs[VanillaBlockIDs.ORE_COAL]    = Rectangle( 2/16f,  2/16f, 1/16f, 1/16f).repeat(CardinalDir.max + 1);

        terrainUVs[VanillaBlockIDs.LOG]         = Rectangle( 4/16f,  1/16f, 1/16f, 1/16f).repeat(CardinalDir.max + 1);
        terrainUVs[VanillaBlockIDs.LOG].yPos    = Rectangle( 5/16f,  1/16f, 1/16f, 1/16f);
        terrainUVs[VanillaBlockIDs.LOG].yNeg    = Rectangle( 5/16f,  1/16f, 1/16f, 1/16f);

        terrainUVs[VanillaBlockIDs.LEAVES]      = Rectangle( 5/16f,  3/16f, 1/16f, 1/16f).repeat(CardinalDir.max + 1);
        terrainUVs[VanillaBlockIDs.SPONGE]      = Rectangle( 0/16f,  3/16f, 1/16f, 1/16f).repeat(CardinalDir.max + 1);
        terrainUVs[VanillaBlockIDs.GLASS]       = Rectangle( 1/16f,  3/16f, 1/16f, 1/16f).repeat(CardinalDir.max + 1);
        terrainUVs[VanillaBlockIDs.ORE_LAPIS]   = Rectangle( 0/16f, 10/16f, 1/16f, 1/16f).repeat(CardinalDir.max + 1);
        terrainUVs[VanillaBlockIDs.BLOCK_LAPIS] = Rectangle( 0/16f,  9/16f, 1/16f, 1/16f).repeat(CardinalDir.max + 1);
        terrainUVs[VanillaBlockIDs.ICE]         = Rectangle( 3/16f,  4/16f, 1/16f, 1/16f).repeat(CardinalDir.max + 1);

        terrainUVs[VanillaBlockIDs.GLOWSTONE]   = Rectangle( 9/16f,  6/16f, 1/16f, 1/16f).repeat(CardinalDir.max + 1);


        // GUI
        string guiPath = buildPath(texturePackDir, "gui", "gui.png");
        gui = LoadTexture(guiPath.toStringz());

        guiUVs["reticle"] = Rectangle(243,   3,   9,   9);
    }

    ~this()
    {
        UnloadTexture(terrain);
        UnloadTexture(gui);
    }
}
