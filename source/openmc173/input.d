module openmc173.input;

import bindbc.raylib;


Vector2 omcGetMouseDeltaPosition()
{
    static bool firstTime = true;
    static Vector2 previousPosition = Vector2(0, 0);
    Vector2 currentPosition = GetMousePosition();
    Vector2 mouseDelta = Vector2(
        currentPosition.x - previousPosition.x,
        currentPosition.y - previousPosition.y
    );
    previousPosition = currentPosition;

    if (firstTime)
    {
        firstTime = false;
        return Vector2(0, 0);
    }

    return mouseDelta;
}
