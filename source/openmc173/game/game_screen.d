module openmc173.game.game_screen;

import bindbc.raylib;
import openmc173.datatypes.section;
import openmc173.datatypes.vector3i;
import openmc173.game.game_camera;
import openmc173.game.world;
import openmc173.input;
import openmc173.io.textures;
import openmc173.raylib_ext.vector3;
import openmc173.screen;
import openmc173.util.datetime;
import openmc173.util.graphics;
import std.algorithm;
import std.conv;
import std.file;
import std.math;
import std.path;
import std.range;
import std.string;


class GameScreen : Screen
{
    private Textures textures;

    private real longitudinalMovementSpeed = 3.0;
    private real lateralMovementSpeed = 3.0;
    private real verticalMovementSpeed = 3.0;
    private real sprintMultiplier = 4.0;
    private real yawSpeed = 1 / 500f;
    private real pitchSpeed = 1 / 500f;

    private Vector3i prevCameraSection;

    private GameCamera camera;
    private World world;

    this(Textures textures)
    {
        this.textures = textures;

        camera = new GameCamera();
        DisableCursor();

        world = new World(textures, camera.position);
    }

    override void onRenderLoop(float delta)
    {
        // Move Camera
        Vector3 movementDirection = Vector3(0.0f, 0.0f, 0.0f);
        Vector3 forwardDirectionUnit = camera.dirOnXZ();
        Vector3 rightDirection = forwardDirectionUnit.cross(camera.up);
        Vector3 rightDirectionUnit = rightDirection.unit();

        if (IsKeyDown(KeyboardKey.KEY_W) ^ IsKeyDown(KeyboardKey.KEY_S))
        {
            if (IsKeyDown(KeyboardKey.KEY_W))
            {
                movementDirection = movementDirection.add(forwardDirectionUnit.scale(longitudinalMovementSpeed));
            }
            else
            {
                movementDirection = movementDirection.add(forwardDirectionUnit.scale(longitudinalMovementSpeed * -1));
            }
        }

        if (IsKeyDown(KeyboardKey.KEY_A) ^ IsKeyDown(KeyboardKey.KEY_D))
        {
            if (IsKeyDown(KeyboardKey.KEY_A))
            {
                movementDirection = movementDirection.add(rightDirectionUnit.scale(lateralMovementSpeed * -1));
            }
            else
            {
                movementDirection = movementDirection.add(rightDirectionUnit.scale(lateralMovementSpeed));
            }
        }

        if (IsKeyDown(KeyboardKey.KEY_SPACE) ^ IsKeyDown(KeyboardKey.KEY_LEFT_SHIFT))
        {
            if (IsKeyDown(KeyboardKey.KEY_SPACE))
            {
                movementDirection = movementDirection.add(camera.up.scale(verticalMovementSpeed));
            }
            else
            {
                movementDirection = movementDirection.add(camera.up.scale(verticalMovementSpeed * -1));
            }
        }

        if (IsKeyDown(KeyboardKey.KEY_Q))
        {
            movementDirection = movementDirection.scale(sprintMultiplier);
        }

        bool destroyTarget;
        if (IsMouseButtonPressed(MouseButton.MOUSE_BUTTON_LEFT))
        {
            destroyTarget = true;
        }

        bool takeScreenshot;
        if (IsKeyPressed(KeyboardKey.KEY_F2))
        {
            takeScreenshot = true;
        }

        camera.moveBy(movementDirection.scale(delta));

        Vector3i currentCameraSection = Vector3i(
            quantize!(floor)(camera.position.x, 16.0).to!int / 16,
            quantize!(floor)(camera.position.y, 16.0).to!int / 16,
            quantize!(floor)(camera.position.z, 16.0).to!int / 16,
        );

        if (prevCameraSection != currentCameraSection)
        {
            world.sectionOnion.moveTo(currentCameraSection);
        }

        prevCameraSection = currentCameraSection;


        // Rotate camera
        Vector2 mouseDelta = omcGetMouseDeltaPosition();
        camera.rotateYaw(-mouseDelta.x * yawSpeed);
        camera.rotatePitch(-mouseDelta.y * pitchSpeed);


        // Find targeted block
        Ray ray = GetMouseRay(Vector2(GetScreenWidth() / 2.0f, GetScreenHeight() / 2.0f), camera.asRaylibCamera);
        RayCollision closestHit = world.castRayAtBlocks(ray);
        Vector3i targetedBlockCoordinates;
        bool isTargetingBlock = closestHit.hit && closestHit.distance <= 5;
        bool shouldRenderTargetWireframe = false;

        if (isTargetingBlock)
        {
            targetedBlockCoordinates = getCubeOfRayHit(closestHit);

            // Destroy targeted block
            if (destroyTarget)
            {
                world.destroyBlockAt(targetedBlockCoordinates, textures);
            }
            else
            {
                shouldRenderTargetWireframe = !destroyTarget;
            }
        }


        // Sort triangles in translucent buffer
        foreach (Section* section; world.sectionOnion.sortedSections(camera.position))
        {
            if (section.translucentModel.meshes !is null)
            {
                Mesh mesh = section.translucentModel.meshes[0];

                sortTrianglesByDistanceSinglePass(
                    cast(Vector3*) mesh.vertices,
                    mesh.indices,
                    mesh.triangleCount,
                    camera.position
                );

                // 6 is the index buffer. Shouldn't there be some nice
                // constant somewhere?
                // TODO: Seems weird that we have to pass in both
                // `mesh` and a pointer to the data. Why doesn't it
                // just use its own pointer?
                UpdateMeshBuffer(mesh, 6, mesh.indices, (mesh.triangleCount * 3 * ushort.sizeof).to!int, 0);
            }
        }


        BeginDrawing();
        ClearBackground(SKYBLUE);


        // Draw the world
        world.draw(camera, textures);

        // Draw target wireframe
        if (shouldRenderTargetWireframe)
        {
            Vector3 selectedCubeCenter = targetedBlockCoordinates.add(Vector3(0.5, 0.5, 0.5));

            BeginMode3D(camera.asRaylibCamera);
            DrawCubeWires(selectedCubeCenter, 1.01, 1.01, 1.01, BLACK);
            EndMode3D();
        }

        // Draw HUD

        DrawFPS(0, 0);
        DrawTexturePro(
            textures.gui,
            textures.guiUVs["reticle"],
            Rectangle(
                (GetScreenWidth() / 2) - textures.guiUVs["reticle"].width,
                (GetScreenHeight() / 2) - textures.guiUVs["reticle"].height,
                2 * textures.guiUVs["reticle"].width,
                2 * textures.guiUVs["reticle"].height
            ),
            Vector2(0, 0),
            0f,
            WHITE
        );

        int textOffset = 25;
        DrawText("Position: [% 3.3f, % 3.3f, % 3.3f]".format(camera.position.x, camera.position.y, camera.position.z).toStringz, 0, textOffset, 20, WHITE); textOffset += 20;
        DrawText("Section: [% 3d, % 3d, % 3d]".format(currentCameraSection.x, currentCameraSection.y, currentCameraSection.z).toStringz, 0, textOffset, 20, WHITE); textOffset += 20;
        DrawText("Velocity: [% 3.3f, % 3.3f, % 3.3f]".format(movementDirection.x, movementDirection.y, movementDirection.z).toStringz, 0, textOffset, 20, WHITE); textOffset += 20;
        DrawText("pitch: %1.3f°".format(camera.pitch / (2 * PI) * 360).toStringz, 0, textOffset, 20, WHITE); textOffset += 20;
        DrawText("yaw: %1.3f°".format(camera.yaw / (2 * PI) * 360).toStringz, 0, textOffset, 20, WHITE); textOffset += 20;
        DrawText("Mouse Delta: [% 4d, % 4d]".format(mouseDelta.x.to!int, mouseDelta.y.to!int).toStringz, 0, textOffset, 20, WHITE); textOffset += 20;
        DrawText("Dirty Sections: % 4d".format(world.sectionOnion.flatConst.filter!(s => s.dirty).walkLength).toStringz, 0, textOffset, 20, WHITE); textOffset += 20;

        if (isTargetingBlock)
        {
            DrawText("Target point:          [% 3.3f, % 3.3f, % 3.3f]".format(closestHit.point.x, closestHit.point.y, closestHit.point.z).toStringz, 0, textOffset, 20, WHITE); textOffset += 20;
            DrawText("Target ray normal:     [% 3.3f, % 3.3f, % 3.3f]".format(closestHit.normal.x, closestHit.normal.y, closestHit.normal.z).toStringz, 0, textOffset, 20, WHITE); textOffset += 20;
            DrawText("Target distance:       % 3.3f".format(closestHit.distance).toStringz, 0, textOffset, 20, WHITE); textOffset += 20;
            DrawText("Target block position: [% 3d, % 3d, % 3d]".format(targetedBlockCoordinates.x, targetedBlockCoordinates.y, targetedBlockCoordinates.z).toStringz, 0, textOffset, 20, WHITE); textOffset += 20;
        }

        EndDrawing();

        if (takeScreenshot)
        {
            string screenshotsDir = thisExePath().dirName().buildPath("screenshots");

            if (!exists(screenshotsDir))
            {
                mkdir(screenshotsDir);
            }
            else if (!isDir(screenshotsDir))
            {
                throw new Exception("Failed to take a screenshot: A file exists in the game directory with the name `screenshots`.");
            }

            TakeScreenshot(buildPath(screenshotsDir, getTimestampForPath("%F %T.%g", false) ~ ".png").toStringz);
        }
    }

    ~this()
    {
        destroy(world);
    }
}


/// Given a ray collision, return the coordinates of the block being looked at.
private static Vector3i getCubeOfRayHit(RayCollision rayCollision)
in (rayCollision.hit, "getCubeOfRayHit requires the rayCollision to contain a hit")
{
    Vector3 targetedBlockCenter =
        rayCollision.point.subtract(
            Vector3(
                rayCollision.normal.x * 0.5f,
                rayCollision.normal.y * 0.5f,
                rayCollision.normal.z * 0.5f,
            )
        );

    return Vector3i(
        floor(targetedBlockCenter.x).to!int,
        floor(targetedBlockCenter.y).to!int,
        floor(targetedBlockCenter.z).to!int,
    );
}
