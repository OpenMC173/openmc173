module openmc173.game.world;

import bindbc.raylib;
import enumap;
import openmc173.datatypes.block;
import openmc173.datatypes.directions;
import openmc173.datatypes.ndarray;
import openmc173.datatypes.section;
import openmc173.datatypes.section_onion;
import openmc173.datatypes.vector3i;
import openmc173.game.game_camera;
import openmc173.io.textures;
import openmc173.util.memory;
import std.algorithm;
import std.array : array;
import std.conv : to;
import std.math;
import std.range;


class World
{
    /// Represents the radius around the central section to render,
    /// including the current section.
    /// - 0 means "don't render anything"
    /// - 1 means "show only the current section"
    /// - 2 means "show the current section and the 26 neighbours"
    size_t renderDistance = 5;

    SectionOnion sectionOnion;

    this(Textures textures, Vector3 playerPosition)
    {
        Vector3i playerSection = Vector3i(
            quantize!(floor)(playerPosition.x, 16.0).to!int / 16,
            quantize!(floor)(playerPosition.y, 16.0).to!int / 16,
            quantize!(floor)(playerPosition.z, 16.0).to!int / 16,
        );

        sectionOnion = new SectionOnion(renderDistance, playerSection, &loadSection, &saveSection);
    }

    Block destroyBlockAt(Vector3i position, Textures textures)
    {
        int targetBlockSectionX = floor(position.x / 16.0).to!int;
        int targetBlockSectionY = floor(position.y / 16.0).to!int;
        int targetBlockSectionZ = floor(position.z / 16.0).to!int;
        Section* targetBlockSection = sectionOnion.getSectionAtGlobalCoords(Vector3i(targetBlockSectionX, targetBlockSectionY, targetBlockSectionZ));
        size_t blockLocalX = ((position.x % 16) + 16) % 16;
        size_t blockLocalY = ((position.y % 16) + 16) % 16;
        size_t blockLocalZ = ((position.z % 16) + 16) % 16;
        return targetBlockSection.setBlock(blockLocalX, blockLocalY, blockLocalZ, Block(VanillaBlockIDs.AIR), textures);
    }

    void draw(GameCamera camera, Textures textures)
    {
        int maxLoadedPerFrame = 1;
        int numLoadedThisFrame;

        foreach (Section* section; sectionOnion.sortedSections(camera.position))
        {
            if (numLoadedThisFrame < maxLoadedPerFrame && section.dirty)
            {
                section.initializeModel(textures, Opacity.opaque);
                section.initializeModel(textures, Opacity.translucent);
                numLoadedThisFrame++;
            }

            if (numLoadedThisFrame >= maxLoadedPerFrame)
            {
                break;
            }
        }

        BeginMode3D(camera.asRaylibCamera);

        foreach (Section* section; sectionOnion.sortedSections(camera.position).retro)
        {
            DrawModel(section.opaqueModel, Vector3(0, 0, 0), 1, WHITE);
        }

        foreach (Section* section; sectionOnion.sortedSections(camera.position).retro)
        {
            DrawModel(section.translucentModel, Vector3(0, 0, 0), 1, WHITE);
        }

        EndMode3D();
    }

    /// Casts `ray` at the blocks in the world, and returns the
    /// RayCollision of the first block along the ray. If no block is
    /// encountered, returns RayCollision.init (where .hit = false).
    RayCollision castRayAtBlocks(Ray ray)
    {
        // retro because sectionsByDistance returns farthest-to-nearest
        // for translucency rendering purposes.

        // take(8) so that the loop only checks the 8 nearest sections,
        // because that's the max number of sections you can "reach" with
        // your "hand" if you were at the intersection of 8 sections.
        // This assumes your reach is 8 blocks or less. If it was bigger,
        // the nearest 27 sections would have to be checked, etc.
        // In those cases, it might be desirable to only check sections that
        // are in the same general direction as the ray.
        // (that is, section x, y, z all >= camera x,y,z (assuming all > 0))

        auto sections = sectionOnion.sortedSections(ray.position).take(8);

        RayCollision closestHit;
        foreach (Section* section; sections)
        {
            void findClosestHitInModel(Model model)
            {
                if (model.meshCount > 0)
                {
                    Mesh mesh = model.meshes[0];

                    for (size_t i = 0; i < mesh.triangleCount * 3; i += 3)
                    {
                        float p1x = mesh.vertices[(mesh.indices[i] * 3)];
                        float p1y = mesh.vertices[(mesh.indices[i] * 3) + 1];
                        float p1z = mesh.vertices[(mesh.indices[i] * 3) + 2];
                        Vector3 p1 = Vector3(p1x, p1y, p1z);

                        float p2x = mesh.vertices[(mesh.indices[i + 1] * 3)];
                        float p2y = mesh.vertices[(mesh.indices[i + 1] * 3) + 1];
                        float p2z = mesh.vertices[(mesh.indices[i + 1] * 3) + 2];
                        Vector3 p2 = Vector3(p2x, p2y, p2z);

                        float p3x = mesh.vertices[(mesh.indices[i + 2] * 3)];
                        float p3y = mesh.vertices[(mesh.indices[i + 2] * 3) + 1];
                        float p3z = mesh.vertices[(mesh.indices[i + 2] * 3) + 2];
                        Vector3 p3 = Vector3(p3x, p3y, p3z);

                        RayCollision currentHit = GetRayCollisionTriangle(ray, p1, p2, p3);

                        if (currentHit.hit && (closestHit.distance.isNaN || currentHit.distance < closestHit.distance))
                        {
                            closestHit = currentHit;
                        }
                    }
                }
            }

            findClosestHitInModel(section.opaqueModel);
            findClosestHitInModel(section.translucentModel);

            if (closestHit.hit)
            {
                break;
            }
        }

        return closestHit;
    }

    Section loadSection(Vector3i globalCoords)
    {
        import std.file : exists, isFile, mkdirRecurse, read;
        import std.format : format;
        import std.path : buildPath;

        string sectionFolderPath = "saves/test-world/sections";
        mkdirRecurse(sectionFolderPath);

        string sectionFile = sectionFolderPath.buildPath(format("%d.%d.%d.section", globalCoords.x, globalCoords.y, globalCoords.z));

        if (exists(sectionFile) && isFile(sectionFile))
        {
            NDarray!(Block, 16, 16, 16) blocks;
            Block[] sectionContents = cast(Block[]) read(sectionFile);
            blocks.setData(sectionContents);

            return Section(globalCoords, blocks, Enumap!(CardinalDir, Section*)());
        }


        NDarray!(Block, 16, 16, 16) blocks;
        foreach (x, y, z, ref block; blocks)
        {
            block = Block(
                // A central section with a tiny ice cube, surrounded
                // by sections of large ice cubes. For testing
                // translucency between sections.
                // (globalCoords.x == 0 && globalCoords.y == 0 && globalCoords.z == 0) ?
                //     ((x >= 7 && x < 9 && y >= 7 && y < 9 && z >= 7 && z < 9) ?
                //         VanillaBlockIDs.ICE :
                //         VanillaBlockIDs.AIR) :
                // (x >= 2 && x < 14 && y >= 2 && y < 14 && z >= 2 && z < 14) ? VanillaBlockIDs.ICE :
                // VanillaBlockIDs.AIR

                // 6x6 hollow cubes of each block type, one per onion layer
                // (infinite onion with center at [0,0,0] always), skipping
                // block ID 0 (air).
                ((x == 5 || x == 10) && y <= 10 && y >= 5 && z <= 10 && z >= 5) ||
                ((y == 5 || y == 10) && x <= 10 && x >= 5 && z <= 10 && z >= 5) ||
                ((z == 5 || z == 10) && x <= 10 && x >= 5 && y <= 10 && y >= 5) ?
                (max(abs(globalCoords.x), abs(globalCoords.y), abs(globalCoords.z)) + 1).to!VanillaBlockIDs :
                VanillaBlockIDs.AIR

                // Stone encased in ice
                // ((x == 4 || x == 12) && y <= 12 && y >= 4 && z <= 12 && z >= 4) ||
                // ((y == 4 || y == 12) && x <= 12 && x >= 4 && z <= 12 && z >= 4) ||
                // ((z == 4 || z == 12) && x <= 12 && x >= 4 && y <= 12 && y >= 4) ?
                // VanillaBlockIDs.ICE :
                // ((x == 6 || x == 10) && y <= 10 && y >= 6 && z <= 10 && z >= 6) ||
                // ((y == 6 || y == 10) && x <= 10 && x >= 6 && z <= 10 && z >= 6) ||
                // ((z == 6 || z == 10) && x <= 10 && x >= 6 && y <= 10 && y >= 6) ?
                // VanillaBlockIDs.STONE :
                // VanillaBlockIDs.AIR

                // Each section is a "realistic" section with grass, dirt, stone, bedrock, and a tree stump.
                // y == 0 ? VanillaBlockIDs.BEDROCK : (
                //     y < 8 ? VanillaBlockIDs.STONE : (
                //         y < 11 ? VanillaBlockIDs.DIRT : (
                //             y < 12 ? VanillaBlockIDs.GRASS : (
                //                 VanillaBlockIDs.AIR
                //             )
                //         )
                //     )
                // )
            );
        }

        return Section(globalCoords, blocks, Enumap!(CardinalDir, Section*)());
    }

    void saveSection(const Section* section)
    {
        import std.file : mkdirRecurse, write;
        import std.format : format;
        import std.path : buildPath;

        string sectionFolderPath = "saves/test-world/sections";
        mkdirRecurse(sectionFolderPath);

        string sectionFile = sectionFolderPath.buildPath(format("%d.%d.%d.section", section.position.x, section.position.y, section.position.z));
        write(sectionFile, cast(void[]) (section.blocks.flatConst));
    }

    ~this()
    {
        assertNotInGC!this;

        foreach (const ref Section section; sectionOnion.flatConst)
        {
            saveSection(&section);
        }
    }
}
