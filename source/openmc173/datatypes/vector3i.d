module openmc173.datatypes.vector3i;

import bindbc.raylib;


struct Vector3i
{
    int x;
    int y;
    int z;
}

/// Adds two `Vector3i`s
Vector3i add(Vector3i v1, Vector3i v2)
{
    return Vector3i(
        v1.x + v2.x,
        v1.y + v2.y,
        v1.z + v2.z,
    );
}

/// Adds an integer to a vector
Vector3i add(Vector3i v, int i)
{
    return Vector3i(
        v.x + i,
        v.y + i,
        v.z + i,
    );
}

/// Subtracts the second `Vector3i` from the first.
Vector3i subtract(Vector3i v1, Vector3i v2)
{
    return Vector3i(
        v1.x - v2.x,
        v1.y - v2.y,
        v1.z - v2.z,
    );
}

/// Subtracts an integer from a vector
Vector3i subtract(Vector3i v, int i)
{
    return Vector3i(
        v.x - i,
        v.y - i,
        v.z - i,
    );
}
