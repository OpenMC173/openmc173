module openmc173.datatypes.ndarray;

import openmc173.util.range : staticIota;
import std.algorithm.iteration;
import std.algorithm.searching;
import std.conv : to;
import std.range : iota, repeat;
import std.string : format;
import std.traits;
import std.typecons : reverse, tuple;


/// A generic n-dimensional array type.
///
/// Basic Usage:
///
///     NDarray!(int, 3, 4, 5) myArray;
///     myArray.setData( [...] );
///     assert(myArray.CAPACITY == 3 * 4 * 5);
///     int[] flatView = myArray.flat;
///     int last = myArray[2, 3, 4];
///
///     foreach (x, y, z, ref element; myArray)
///     {
///         element = x + y + z;
///     }
///
///     foreach (element; myArray)
///     {
///         assert(element >= 0);
///     }
///
///
/// Advanced Usage:
///
///     - axisOrder: Used to tell the struct how to interpret the underlying 1D
///                  array in N-dimensions. See documentation of `axisOrder` for
///                  details.
///
/// Known Issues:
///     - an NDarray of type `char` seems to be a bit buggy, I think because of
///       UTF encoding and the fact that char.init == 'xFF'. Not sure if wchar
///       and dchar have the same issues.
///
/// TODO: Can we make this work with range functions? Perhaps via `alias this`?
public struct NDarray(E, DIMENSIONS...)
if
(
    DIMENSIONS.length > 0 &&
    all!((e) => isImplicitlyConvertible!(typeof(e), size_t))([DIMENSIONS]) &&
    all!((e) => e > 0)([DIMENSIONS])
)
{
    enum N = DIMENSIONS.length;
    enum CAPACITY = [ DIMENSIONS ].fold!((size_t a, size_t b) => a * b);
    static immutable(size_t[N]) dimensions = [ DIMENSIONS ];

    private E[CAPACITY] data;
    alias RawType = typeof(data);
    version(unittest) ref E[CAPACITY] getData() { return data; }

    /// Maps an array level (0 = outermost, $ = innermost) to a position in [DIMENSIONS].
    ///
    /// Describes the semantic order of the internal flat array's axes.
    /// In 2D arrays, this would be called "row-major" vs "column-major" order.
    /// By default, it's assumed that the "outermost" axis has a length of
    /// DIMENSIONS[0] and the "innermost" axis has a length of DIMENSIONS[$-1].
    /// Note that this makes an NDarray declaration look like a backwards
    /// "vanilla" multi-dimensional array declaration:
    ///     int[11][5][3] foo;
    /// has the same memory layout as:
    ///     NDarray!(int, 3, 5, 11) foo;
    /// This has the nice benefit of making NDarray declaration syntax match
    /// the opIndex syntax:
    ///     NDarray!(int, 3, 5, 11) foo;
    ///     foo[2, 4, 10];  // gets the last element of the array.
    /// However, the ordering can be overridden if the underlying data
    /// is stored in an order that is not intuitive (e.g. a cube where the
    /// innermost axis represents the Y coordinate of the cube). In this case,
    /// axisOrder comes to the rescue. If N = 3 and axisOrder = [1, 2, 0], then
    /// the innermost axis is actually DIMENSIONS[1] in size, the next-innermost
    /// axis is DIMENSIONS[2] in size, and the outermost axis is DIMENSIONS[0]
    /// in size.
    public size_t[N] axisOrder = staticIota!(size_t, N);
    invariant (axisOrder[].all!((e) => e < N));
    invariant (axisOrder[].uniq.count == axisOrder.length);

    this(size_t[N] axisOrder)
    do
    {
        this.axisOrder = axisOrder;
    }

    /// Sets the contents of this array to the contents of `data`.
    /// `data` may not be larger than `this.CAPACITY`, but it may be smaller.
    /// If it is smaller, then only the contents in the range [0..`data.length`)
    /// will be overwritten, and the remaining contents will be unchanged.
    public void setData(E[] data)
    in(data.length <= CAPACITY)
    do
    {
        this.data[0..data.length] = data;
        this.axisOrder = axisOrder;
    }

    /**
     * Returns a 1-D slice of the NDarray contents.
     *
     * Points at the actual array contents, so modifying the returned slice
     * will modify the contents of the NDarray.
     */
    public E[] flat()
    {
        E[] retVal = data;
        return retVal;
    }

    /// Returns a read-only 1-D slice of the NDarray contents.
    public const(E[]) flatConst() const
    {
        const(E[]) retVal = data;
        return retVal;
    }


    // Declarations of E get(...) for all possible values of N.
    // The 2D/3D versions are just what generateOpIndexFunction() would return,
    // except the parameter names are x, y, z instead of idx0, idx1, idx2.
    static if (N == 1)
    {
        ref E opIndex(size_t i)
        {
            return data[i];
        }
    }
    else static if (N == 2)
    {
        ref E opIndex(size_t x, size_t y)
        {
            size_t[N] params = [x, y];
            size_t idx;
            idx += params[axisOrder[0]] * dimensions[axisOrder[1]];
            idx += params[axisOrder[1]];
            return data[idx];
        }
    }
    else static if (N == 3)
    {
        ref E opIndex(size_t x, size_t y, size_t z)
        {
            size_t[N] params = [x, y, z];
            size_t idx;
            idx += params[axisOrder[0]] * dimensions[axisOrder[2]] * dimensions[axisOrder[1]];
            idx += params[axisOrder[1]] * dimensions[axisOrder[2]];
            idx += params[axisOrder[2]];
            return data[idx];
        }
    }
    else
    {
        mixin(generateOpIndexFunction(N));
    }


    /// foreach will always iterate in the order the data is stored internally,
    /// irrespective of the value of axisOrder.
    public int opApply(int delegate(ref E) loop)
    {
        int result = 0;
        foreach (ref E element; data)
        {
            result = loop(element);
            if (result != 0) break;
        }
        return result;
    }


    /// ditto
    public int opApplyReverse(int delegate(ref E) loop)
    {
        int result = 0;
        foreach_reverse (ref E element; data)
        {
            result = loop(element);
            if (result != 0) break;
        }
        return result;
    }


    // Indexed versions of foreach for all possible values of N.
    static if (N == 1)
    {
        public int opApply(int delegate(size_t, ref E) loop)
        {
            int result = 0;
            foreach (i, ref E element; data)
            {
                result = loop(i, element);
                if (result != 0) break;
            }
            return result;
        }

        public int opApplyReverse(int delegate(size_t, ref E) loop)
        {
            int result = 0;
            foreach_reverse (i, ref E element; data)
            {
                result = loop(i, element);
                if (result != 0) break;
            }
            return result;
        }
    }
    else
    {
        mixin(generateOpApply(N, false));
        mixin(generateOpApply(N, true));
    }


    /// Returns the multi-dimensional index corresponding to the given index.
    ///
    /// Given a 2*2 array, indexToCoords(2) returns:
    /// [1, 0] when axisOrder is [0, 1] (xy)
    /// [0, 1] when axisOrder is [1, 0] (yx)
    public size_t[N] indexToCoords(size_t i) const
    {
        size_t[N] coords;

        foreach (n; 0..N)
        {
            int sizeOfLowerDims = 1;
            foreach (d; n+1 .. N)
            {
                sizeOfLowerDims *= dimensions[axisOrder[d]];
            }
            coords[axisOrder[n]] = (i % (sizeOfLowerDims * dimensions[axisOrder[n]])) / sizeOfLowerDims;
        }

        return coords;
    }

    public size_t opDollar(size_t pos)()
    {
        return dimensions[pos];
    }
}


/************************************
 *                                  *
 *         Helper Functions         *
 *                                  *
 ************************************/

string generateOpIndexFunction(size_t dimensions)
in (dimensions >= 1)
{
    string def = "ref E opIndex(";
    for (int i = 0; i < dimensions; i++)
    {
        def ~= "size_t idx%d".format(i);
        if (i < dimensions - 1) def ~= ", ";
    }
    def ~= ") { size_t idx; size_t[N] params = [";
    for (int i = 0; i < dimensions; i++)
    {
        def ~= "idx%d".format(i);
        if (i < dimensions - 1) def ~= ", ";
    }
    def ~= "]; ";
    for (int i = 0; i < dimensions; i++)
    {
        def ~= "idx += params[axisOrder[%d]]".format(i);
        for (int j = (dimensions - 1).to!int; j > i; j--)
        {
            def ~= " * dimensions[axisOrder[%d]]".format(j);
        }
        def ~= "; ";
    }
    def ~= "return data[idx]; }";
    return def;
}

/// For generating a call to a loop delegate function with an arbitrary number
/// of index loop variables, such as in the following foreach:
///
/// foreach (x, y, z, ref cell; myNdArray) { ... }
string generateLoopCall(string loopDelegateName,
                        string idxArrayName,
                        size_t idxArraySize,
                        string lastArgName)
{
    string argList;
    foreach (i; 0..idxArraySize)
    {
        argList ~= "%s[%d], ".format(idxArrayName, i);
    }

    return loopDelegateName ~ "(" ~ argList ~ lastArgName ~ ")";
}

string generateOpApplyParamType(size_t n)
{
    return "int delegate(" ~ ("size_t, ".repeat(n).fold!"a~=b"("")) ~ "ref E)";
}

private string generateOpApply(size_t n, bool reverse)
{
    // TODO: Once mixin types get released, we can define opApply and opApplyReverse normally, just have the argument as a mixin type.
    return `
    public int opApply%s(%s loop)
    {
        int result = 0;
        foreach%s (i, ref E element; data)
        {
            size_t[N] coords = indexToCoords(i);
            result = %s;
            if (result != 0) break;
        }
        return result;
    }
    `.format(reverse ? "Reverse" : "",
             generateOpApplyParamType(n),
             reverse ? "_reverse" : "",
             generateLoopCall("loop", "coords", n, "element")
            );
    // It's a big-ish function, I can't be bothered to make unittests for this too...
}
