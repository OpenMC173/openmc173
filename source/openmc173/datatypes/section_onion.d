module openmc173.datatypes.section_onion;

import bindbc.raylib;
import enumap;
import exceeds_expectations;
import openmc173.datatypes.block;
import openmc173.datatypes.boxi;
import openmc173.datatypes.directions;
import openmc173.datatypes.ndarray;
import openmc173.datatypes.section;
import openmc173.datatypes.vector3i;
import openmc173.raylib_ext.vector3;
import std.algorithm;
import std.conv;
import std.range;
import std.stdio;
import std.traits;

/++
 + A data structure for storing loaded sections of a world.
 +
 + Consists of an array-of-arrays-of-sections in which each sub-array
 + is a "layer" of the onion. The first array contains only one chunk,
 + the center. The second array contains the 26 (3³-1) neighbours of
 + the first layer (diagonals are included). The third array contains
 + the 98 (5³-3³) neighbours of the second layer, and so on.
 +
 + In the context of a SectionOnion, there are 3 coordinate systems to
 + be aware of:
 +
 + 1. Global Coordinates, which are the coordinates of the section in
 +    the world, independent of where they are placed in the onion.
 + 2. Onion Coordinates, which are the coordinates of the section in
 +    the onion. [0,0,0] is the center section (the only element of
 +    the first sub-array).
 + 3. Layer Coordinates, which are the coordinates of the section in
 +    its layer. [0,0,0] is the bottom-left-back section, and has
 +    index 0. The last element of the layer's array has coordinates
 +    [d-1, d-1, d-1] where d is the diameter of that layer.
 +
 + In all cases, the handedness of the coordinate system is the same
 + as that of Raylib: left-handed and y-up. All coordinates are the
 + coordinates of a section within the world, *not* block coordinates.
 +
 + "Section index" refers to the index of a section within its layer.
 + "Layer index" refers to the index of a layer within the onion,
 + where 1 is the center layer.
 +/
final class SectionOnion
{
    private Section[] sections;
    private Section*[][] layers;

    version(unittest) const(Section*[][]) getLayers() { return layers; }

    alias SectionLoader = Section delegate(Vector3i globalCoords);
    private SectionLoader sectionLoader;

    alias SectionSaver = void delegate(Section* section);
    private SectionSaver sectionSaver;

    Vector3i globalCoordsOfCenter;

    this(
        size_t layerCount,
        Vector3i globalCoordsOfCenter,
        SectionLoader sectionLoader,
        SectionSaver sectionSaver,
    )
    {
        this.globalCoordsOfCenter = globalCoordsOfCenter;
        this.sectionLoader = sectionLoader;
        this.sectionSaver = sectionSaver;

        this.layers.length = layerCount;
        foreach (size_t layerIndex, ref Section*[] layer; layers)
        {
            layer.length = onionLayerArrayLength(layerIndex);
        }

        loadAllSections();
    }

    public void moveTo(Vector3i globalCoords)
    {
        Vector3i oldCenter = this.globalCoordsOfCenter;
        Vector3i newCenter = globalCoords;
        this.globalCoordsOfCenter = newCenter;

        Vector3i[] coordsInOldOnion = listAllGlobalCoordsInOnion(oldCenter, layers.length);
        Vector3i[] coordsInNewOnion = listAllGlobalCoordsInOnion(newCenter, layers.length);

        // Needed for setops.setDifference
        // a is "less" than b if it comes before b according to the XZY axis order:
        // - its y is smaller
        // - or its y is equal but z is smaller
        // - or its y is equal and z is equal but x is smaller
        // In any other case, a is greater than or equal to b.
        static bool coordOrderPredicate(Vector3i a, Vector3i b)
        {
            return
                (a.y < b.y) ||
                (a.y == b.y && a.z < b.z) ||
                (a.y == b.y && a.z == b.z && a.x < b.x);
        }

        debug
        {
            expect(coordsInOldOnion).toSatisfy(arr => arr.isSorted!coordOrderPredicate);
            expect(coordsInNewOnion).toSatisfy(arr => arr.isSorted!coordOrderPredicate);
        }

        auto globalCoordsToSave = setDifference!coordOrderPredicate(coordsInOldOnion, coordsInNewOnion);
        auto globalCoordsToLoad = setDifference!coordOrderPredicate(coordsInNewOnion, coordsInOldOnion);

        debug int saveCount;
        foreach (ref Section section; sections)
        {
            if (globalCoordsToSave.canFind(section.position))
            {
                sectionSaver(&section);
                debug saveCount++;
                destroy(section);

                Vector3i sectionToLoad = globalCoordsToLoad.front;
                section = sectionLoader(sectionToLoad);
                globalCoordsToLoad.popFront();
            }
        }

        debug expect(saveCount).toEqual(walkLength(globalCoordsToSave));
        debug expect(walkLength(globalCoordsToLoad)).toEqual(0);

        rebuildLayers();
        recalculateNeighbours();
    }

    void rebuildLayers()
    {
        foreach (ref Section section; sections)
        {
            Vector3i onionCoords = globalCoordsToOnionCoords(section.position, this.globalCoordsOfCenter);
            Vector3i layerCoords = onionCoordsToLayerCoords(onionCoords);
            size_t layerIndex = onionCoordsToLayerIndex(onionCoords);
            size_t sectionIndex = layerCoordsToSectionIndex(layerIndex, layerCoords);
            layers[layerIndex][sectionIndex] = &section;
        }
    }

    void loadAllSections()
    in(sections.length == 0)
    out { assert(sections.length == numberOfSectionsInOnion(layers.length)); }
    do
    {
        foreach (Vector3i globalCoord; listAllGlobalCoordsInOnion(globalCoordsOfCenter, layers.length))
        {
            sections ~= sectionLoader(globalCoord);
        }

        rebuildLayers();
        recalculateNeighbours();
    }

    void recalculateNeighbours()
    {
        foreach (Section*[] layer; layers)
        {
            foreach (Section* section; layer)
            {
                section.neighbours =
                    Enumap!(CardinalDir, Section*)([
                        getSectionAtGlobalCoords(section.position.add(Vector3i( 1,  0,  0))),
                        getSectionAtGlobalCoords(section.position.add(Vector3i(-1,  0,  0))),
                        getSectionAtGlobalCoords(section.position.add(Vector3i( 0,  1,  0))),
                        getSectionAtGlobalCoords(section.position.add(Vector3i( 0, -1,  0))),
                        getSectionAtGlobalCoords(section.position.add(Vector3i( 0,  0,  1))),
                        getSectionAtGlobalCoords(section.position.add(Vector3i( 0,  0, -1))),
                    ]);
            }
        }
    }

    // TODO: Call this eg "fully sorted sections" and add another method called eg "semi sorted sections" which just joins the layers together, not sorting within each layer.
    /// Return a flattened InputRange of `Section*` in order from nearest to
    /// farthest.
    ///
    /// I suspect this may be buggy. Had some issues while trying to debug
    /// `loadAllSections` above.
    public auto sortedSections(Vector3 pos)
    out(retVal; isInputRange!(Unqual!(typeof(retVal))), "sortedSections should return an InputRange, but got: " ~ typeid(retVal).to!string)
    out(retVal; is(ElementType!(Unqual!(typeof(retVal))) == Section*), "sortedSections should return range of Section*, but got range of: " ~ typeid(ElementType!(Unqual!(typeof(retVal)))).to!string)
    {
        return layers.map!(layer =>
                layer.sort!(
                    (Section* a, Section* b)
                    {
                        Vector3i aPositionBlocks = Vector3i(16 * a.position.x, 16 * a.position.y, 16 * a.position.z);
                        Vector3i aCenter = aPositionBlocks.add(Vector3i(8, 8, 8));
                        float distanceToA = pos.distanceSquared(Vector3(aCenter.x, aCenter.y, aCenter.z));

                        Vector3i bPositionBlocks = Vector3i(16 * b.position.x, 16 * b.position.y, 16 * b.position.z);
                        Vector3i bCenter = bPositionBlocks.add(Vector3i(8, 8, 8));
                        float distanceToB = pos.distanceSquared(Vector3(bCenter.x, bCenter.y, bCenter.z));

                        return distanceToA < distanceToB;
                    }
                )
            ).joiner;
    }

    /// Returns a pointer to section at global coords `coords`, or
    /// `null` if the coordinates are out of bounds.
    public Section* getSectionAtGlobalCoords(Vector3i coords)
    {
        return getSectionAtOnionCoords(globalCoordsToOnionCoords(coords, globalCoordsOfCenter));
    }

    /// Returns the global coordinates of the given onion coordinates.
    private Vector3i onionCoordsToGlobalCoords(Vector3i onionCoords)
    {
        return globalCoordsOfCenter.add(onionCoords);
    }

    bool containsGlobalCoords(Vector3i globalCoords)
    {
        size_t layerCount = layers.length;
        if (layerCount == 0) return false;

        // Reminder: min bound is inside, max bound should be just outside hence the +1
        int max =  (onionLayerDiameter(layerCount - 1).to!int / 2) + 1;
        int min = -(onionLayerDiameter(layerCount - 1).to!int / 2);

        BoxI onionExtents = BoxI(
            Vector3i(min, min, min).add(globalCoordsOfCenter),
            Vector3i(max, max, max).add(globalCoordsOfCenter),
        );

        return onionExtents.contains(globalCoords);
    }

    /// Returns a pointer to section at onion coords `coords`, or
    /// `null` if the coordinates are out of bounds.
    Section* getSectionAtOnionCoords(Vector3i onionCoords)
    do
    {
        if (!containsGlobalCoords(onionCoordsToGlobalCoords(onionCoords))) return null;

        size_t layer = onionCoordsToLayerIndex(onionCoords);
        Vector3i coordsInLayer = onionCoords.add(layer.to!int);
        size_t indexInLayer = layerCoordsToSectionIndex(layer, coordsInLayer);

        return layers[layer][indexInLayer];
    }

    public const(Section[]) flatConst()
    {
        return sections;
    }
}

size_t onionLayerDiameter(size_t layer)
pure
out(result; result % 2 == 1, "Expected onionLayerDiameter to return an odd number but got " ~ result.to!string)
{
    return layer * 2 + 1;
}

size_t onionLayerArrayLength(size_t layer)
pure
{
    if (layer == 0) return 1;
    return onionLayerDiameter(layer)^^3 - onionLayerDiameter(layer - 1)^^3;
}

size_t numberOfSectionsInOnion(size_t layerCount)
pure
{
    size_t result = 0;
    foreach (size_t layer; 0 .. layerCount)
    {
        result += onionLayerArrayLength(layer);
    }

    return result;
}


/// Given a layer and an index within that layer's array, return the
/// XYZ coordinates of the section. Count up first by X, then by Z,
/// then by Y.
///
/// Examples:
///     ```d
///     sectionIndexToLayerCoords(1, 0) == Vector3i(0, 0, 0);
///     sectionIndexToLayerCoords(2, 0) == Vector3i(0, 0, 0);
///     sectionIndexToLayerCoords(2, 1) == Vector3i(1, 0, 0);
///     sectionIndexToLayerCoords(2, 2) == Vector3i(2, 0, 0);
///     sectionIndexToLayerCoords(2, 3) == Vector3i(0, 0, 1);
///     sectionIndexToLayerCoords(2, 9) == Vector3i(0, 1, 0);
///     sectionIndexToLayerCoords(2, 25) == Vector3i(2, 2, 2);
///     ```
///
/// See_Also:
///     [layerCoordsToSectionIndex]
Vector3i sectionIndexToLayerCoords(size_t layer, size_t index)
{
    if (layer == 0)
    {
        return Vector3i(0, 0, 0);
    }

    size_t diameter = onionLayerDiameter(layer);
    assert(diameter >= 3, "Sanity check to make sure we can subtract 2 when calcualting intermediateLevelCount");

    assert(
        index < onionLayerArrayLength(layer),
        "sectionIndexToLayerCoords received index " ~ index.to!string ~ " but onionLayerArrayLength is " ~ onionLayerArrayLength(layer).to!string
    );

    size_t bottomLevelSize = onionLayerDiameter(layer)^^2;
    size_t intermediateLevelSize = (onionLayerDiameter(layer) - 1) * 4;
    size_t intermediateLevelCount = diameter - 2;

    // Bottom level
    if (index < bottomLevelSize) return Vector3i((index % diameter).to!int, 0, (index / diameter).to!int);

    // Intermediate levels
    if (index < bottomLevelSize + (intermediateLevelCount * intermediateLevelSize))
    {

        size_t positionInLevel = (index - bottomLevelSize) % intermediateLevelSize;

        size_t x = (
            // First row
            positionInLevel < diameter ? positionInLevel :

            // Last row
            positionInLevel >= diameter + (intermediateLevelCount * 2) ? (positionInLevel - diameter - (intermediateLevelCount * 2)) % diameter :

            // Any of the middle rows, first of the pair
            positionInLevel % 2 == 1 ? 0 :

            // Any of the middle rows, second of the pair
            diameter - 1
        );

        size_t y = ((index - bottomLevelSize) / intermediateLevelSize) + 1;

        size_t z = (
            // First row
            positionInLevel < diameter ? 0 :

            // Last row
            positionInLevel >= diameter + (intermediateLevelCount * 2) ? diameter - 1 :

            // Any of the middle rows
            ((positionInLevel - diameter) / 2) + 1
        );

        return Vector3i(x.to!int, y.to!int, z.to!int);
    }

    // Top level
    assert(
        index >= bottomLevelSize + (intermediateLevelCount * intermediateLevelSize),
        "Sanity check to make sure we can subtract intermediateLevelCount * intermediateLevelSize when calculating the top level"
    );

    size_t positionInLevel = index - bottomLevelSize - (intermediateLevelCount * intermediateLevelSize);

    return Vector3i(
        (positionInLevel % diameter).to!int,
        (diameter - 1).to!int,
        (positionInLevel / diameter).to!int,
    );
}


/// Given a layer and coordinates index within that layer, return the
/// index within that layer's array. Count up first by X, then by Z,
/// then by Y.
///
/// Examples:
///     ```d
///     layerCoordsToSectionIndex(1, Vector3i(0, 0, 0)) == 0;
///     layerCoordsToSectionIndex(2, Vector3i(0, 0, 0)) == 0;
///     layerCoordsToSectionIndex(2, Vector3i(1, 0, 0)) == 1;
///     layerCoordsToSectionIndex(2, Vector3i(2, 0, 0)) == 2;
///     layerCoordsToSectionIndex(2, Vector3i(0, 0, 1)) == 3;
///     layerCoordsToSectionIndex(2, Vector3i(0, 1, 0)) == 9;
///     layerCoordsToSectionIndex(2, Vector3i(2, 2, 2)) == 25;
///     ```
///
/// See_Also:
///     [sectionIndexToLayerCoords]
size_t layerCoordsToSectionIndex(size_t layer, Vector3i coords)
{
    if (layer == 0)
    {
        return 0;
    }

    size_t diameter = onionLayerDiameter(layer);
    assert(diameter >= 3, "Sanity check to make sure we can subtract 2 when calcualting intermediateLevelCount");

    // assert(
    //     index < calcLayerArrayLength(layer),
    //     "sectionIndexToLayerCoords received index " ~ index.to!string ~ " but shellArrayLength is " ~ calcLayerArrayLength(layer).to!string
    // );

    size_t bottomLevelSize = onionLayerDiameter(layer)^^2;
    size_t intermediateLevelSize = (onionLayerDiameter(layer) - 1) * 4;
    size_t intermediateLevelCount = diameter - 2;

    // Bottom level
    if (coords.y == 0)
    {
        return coords.x + (coords.z * diameter);
    }

    // Intermediate levels
    if (coords.y > 0 && coords.y < diameter - 1)
    {
        // First row
        if (coords.z == 0)
        {
            return
                bottomLevelSize +
                (coords.y - 1) * intermediateLevelSize +
                coords.x;
        }

        // Last row
        else if (coords.z == diameter - 1)
        {
            return
                bottomLevelSize +
                (coords.y - 1) * intermediateLevelSize +
                diameter +
                2 * (diameter - 2) +
                coords.x;
        }

        // Any of the middle rows, first of the pair
        else if (coords.x == 0)
        {
            return
                bottomLevelSize +
                (coords.y - 1) * intermediateLevelSize +
                diameter +
                (coords.z - 1) * 2;
        }

        // Any of the middle rows, second of the pair
        else if (coords.x == diameter - 1)
        {
            return
                bottomLevelSize +
                (coords.y - 1) * intermediateLevelSize +
                diameter +
                (coords.z - 1) * 2 +
                1;
        }

        else assert(false, "Should not have reached this point, something is wrong in the conditions above.");
    }

    return
        bottomLevelSize +
        intermediateLevelCount * intermediateLevelSize +
        coords.x + (coords.z * diameter);
}


/// Returns the layer containing the given coordinates.
///
/// `Vector3i(0,0,0)` is the center layer.
size_t onionCoordsToLayerIndex(Vector3i coords)
{
    import std.math : abs;

    return max(
        abs(coords.x),
        abs(coords.y),
        abs(coords.z),
    );
}


/// Returns the layer coords of the given onion coords. To find which
/// layer contains them, see [onionCoordsToLayerIndex].
Vector3i onionCoordsToLayerCoords(Vector3i onionCoords)
{
    size_t layerIndex = onionCoordsToLayerIndex(onionCoords);
    return onionCoords.add(layerIndex.to!int);
}


/// Returns the onion coords of the given coords within the given
/// layer.
Vector3i layerCoordsToOnionCoords(size_t layerIndex, Vector3i layerCoords)
{
    return layerCoords.subtract(layerIndex.to!int);
}


/// Returns the onion coordinates of the given global coordinates.
Vector3i globalCoordsToOnionCoords(Vector3i globalCoords, Vector3i globalCoordsOfCenter)
{
    Vector3i onionCoords = globalCoords.subtract(globalCoordsOfCenter);
    return onionCoords;
}


Vector3i[] listAllGlobalCoordsInOnion(Vector3i center, size_t layerCount)
{
    Vector3i[] result;

    foreach (int y; (center.y - layerCount.to!int + 1) .. (center.y + layerCount.to!int))
        foreach (int z; (center.z - layerCount.to!int + 1) .. (center.z + layerCount.to!int))
            foreach (int x; (center.x - layerCount.to!int + 1) .. (center.x + layerCount.to!int))
            {
                result ~= Vector3i(x, y, z);
            }

    return result;
}
