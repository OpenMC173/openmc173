module openmc173.datatypes.boxi;

import openmc173.datatypes.vector3i;


/// max not included
struct BoxI
{
    Vector3i min;
    Vector3i max;
}

bool contains(BoxI box, Vector3i v)
{
    return (
        v.x >= box.min.x && v.x < box.max.x &&
        v.y >= box.min.y && v.y < box.max.y &&
        v.z >= box.min.z && v.z < box.max.z
    );
}
