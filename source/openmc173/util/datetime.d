module openmc173.util.datetime;

import datefmt : datefmt = format;
import std.datetime : Clock, SysTime;
import std.regex : matchFirst, regex, replaceAll;


/// Gets the current system time and returns a string of it formatted using `formatTime`.
string getTimestamp(string formatStr = "%F %T.%g", bool appendTZ = true)
{
    return formatTime(Clock.currTime(), formatStr, appendTZ);
}

/// Same as `getTimestamp`, but ensures the resulting string can exist in a file path.
///
/// See_Also:
///     - replaceInvalidPathChars
///     - getTimestamp
string getTimestampForPath(string format = "%F %T", bool appendTZ = true, string replaceWith = "", bool squashConsecutive = true)
{
    return replaceInvalidPathChars(getTimestamp(format, appendTZ), replaceWith, squashConsecutive);
}

/// Takes a file name and returns a string equivalent to `str`, but any
/// characters that cannot exist in a file pathname will be removed or replaced.
///
/// Note that this means that path separators will be removed, since they are
/// invalid on both Windows and POSIX. Make sure the given string is only
/// a file name, not whole path.
///
/// The result is system dependent, and assumes the system is POSIX-compliant
/// if it's not Windows.
///
/// Params:
///
///     str = The string to clean up.
///
///     replaceWith = Invalid characters will be replaced with this string.
///                   Default is empty string.
///
///     squashConsecutive =  If true, consecutive invalid characters will be
///                          replaced by only one instance of `replaceWith`.
///                          Default is true.
///
string replaceInvalidPathChars(string str, string replaceWith = "", bool squashConsecutive = true)
{
    version(Windows)
    {
        auto toReplace = regex(`[><:"/\\|?*]` ~ (squashConsecutive ? "+" : ""));
    }
    else
    {
        auto toReplace = regex("[/\0]" ~ (squashConsecutive ? "+" : ""));
    }

    if (matchFirst(replaceWith, toReplace))
    {
        throw new Exception("Cannot replace invalid path characters with string \"" ~ replaceWith ~ "\" because it also contains invalid path characters.");
    }

    return replaceAll(str, toReplace, replaceWith);
}


/// Converts an instance of `SysTime` to a string with the following format:
/// YYYY-MM-DD HH:MM:SS TZ
/// where TZ is of the form ±HH:MM (UTC is +00:00).
///
/// Params:
///
///     time = The instance of `SysTime` to format.
///
///     formatStr = Formatting string. See datefmt docs for details.
///                  Default `%F %T.%g`.
///
///     appendTZ = If true, the resulting string will have the time zone
///                 appended at the end. Default true.
///
public string formatTime(SysTime time, string formatStr = "%F %T.%g", bool appendTZ = true)
{
    string retVal = datefmt(time, formatStr);

    if (appendTZ)
    {
        // datefmt normally outputs timezones in the format +hhmm,
        // but ISO says it should be +hh:mm, and I think that's more
        // consistent considering the timestamp is also colon-separated.
        string tz = datefmt(time, "%z");
        retVal ~= " " ~ tz[0..3] ~ ":" ~ tz[3..$];
    }

    return retVal;
}
