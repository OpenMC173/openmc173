module openmc173.util.range;


/// Returns a static array whose contents are
/// [ 0, 1, 2, ... , N ]
public T[N] staticIota(T, size_t N)()
if (is(int : T))
{
    // There are probably better ways of doing this but I think this is good enough.
    import std.meta : aliasSeqOf;
    import std.range : iota;
    return [ aliasSeqOf!(N.iota) ];
}
