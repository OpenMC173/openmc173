module openmc173.util.graphics;

import bindbc.raylib;
import openmc173.raylib_ext.vector3;
import std.algorithm.mutation;
import std.conv;


/// Returns whether `referencePosition` is closer to the triangle
/// `[vertices[first1], vertices[first2], vertices[first3]]`
/// than to the triangle
/// `[vertices[second1], vertices[second2], vertices[second3]]`
bool isFirstTriangleCloserThanSecond(
    const Vector3* vertices, Vector3 referencePosition,
    ushort first1, ushort first2, ushort first3,
    ushort second1, ushort second2, ushort second3
)
{
    float first1DistSqrd = referencePosition.distanceSquared(vertices[first1]);
    float first2DistSqrd = referencePosition.distanceSquared(vertices[first2]);
    float first3DistSqrd = referencePosition.distanceSquared(vertices[first3]);

    float distSqrdOfClosestInFirst = first1DistSqrd;

    if (first2DistSqrd < distSqrdOfClosestInFirst)
    {
        distSqrdOfClosestInFirst = first2DistSqrd;
    }
    if (first3DistSqrd < distSqrdOfClosestInFirst)
    {
        distSqrdOfClosestInFirst = first3DistSqrd;
    }


    float second1DistSqrd = referencePosition.distanceSquared(vertices[second1]);
    float second2DistSqrd = referencePosition.distanceSquared(vertices[second2]);
    float second3DistSqrd = referencePosition.distanceSquared(vertices[second3]);

    float distSqrdOfClosestInSecond = second1DistSqrd;

    if (second2DistSqrd < distSqrdOfClosestInSecond)
    {
        distSqrdOfClosestInSecond = second2DistSqrd;
    }
    if (second3DistSqrd < distSqrdOfClosestInSecond)
    {
        distSqrdOfClosestInSecond = second3DistSqrd;
    }

    return distSqrdOfClosestInFirst < distSqrdOfClosestInSecond;
}

void sortTrianglesByDistance(const Vector3* vertices, ushort* indices, int triangleCount, Vector3 fromPos)
{
    int indexCount = triangleCount * 3;

    for (ushort i = 3; i <= indexCount - 3; i += 3)
    {
        ushort j = i;

        // If it's farther than the predecessor, bubble to the front
        while (
            j >= 3 &&
            // isCurrentFartherThanPredecessor? If so, swap.
            isFirstTriangleCloserThanSecond(
                vertices, fromPos,
                indices[(j - 3).to!ushort], indices[(j - 2).to!ushort], indices[(j - 1).to!ushort],        // predecessor
                indices[                j], indices[(j + 1).to!ushort], indices[(j + 2).to!ushort],        // current
            )
        )
        {
            swapRanges(indices[(j - 3) .. j], indices[j .. (j + 3)]);
            j -= 3;
        }
    }
}

void sortTrianglesByDistanceSinglePass(const Vector3* vertices, ushort* indices, int triangleCount, Vector3 fromPos)
{
    int indexCount = triangleCount * 3;

    // for (ushort i = 3; i <= indexCount - 3; i += 3)
    // {
        ushort j = (indexCount - 3).to!ushort;

        // If it's farther than the predecessor, bubble to the front
        while (j >= 3)
        {
            // isCurrentFartherThanPredecessor? If so, swap.
            if (
                isFirstTriangleCloserThanSecond(
                    vertices, fromPos,
                    indices[(j - 3).to!ushort], indices[(j - 2).to!ushort], indices[(j - 1).to!ushort],        // predecessor
                    indices[                j], indices[(j + 1).to!ushort], indices[(j + 2).to!ushort],        // current
                )
            )
            {
                swapRanges(indices[(j - 3) .. j], indices[j .. (j + 3)]);
            }
            j -= 3;
        }
    // }
}
