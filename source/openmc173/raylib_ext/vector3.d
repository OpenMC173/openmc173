module openmc173.raylib_ext.vector3;

import bindbc.raylib;
import openmc173.datatypes.vector3i;
import std.math;


float magnitude(const Vector3 vec)
{
    return sqrt(
        vec.x * vec.x +
        vec.y * vec.y +
        vec.z * vec.z
    );
}

Vector3 unit(const Vector3 vec)
{
    float mag = vec.magnitude;
    return Vector3(
        vec.x / mag,
        vec.y / mag,
        vec.z / mag,
    );
}

/// Add two vectors
Vector3 add(Vector3 v1, Vector3 v2)
{
    return Vector3(
        v1.x + v2.x,
        v1.y + v2.y,
        v1.z + v2.z,
    );
}

/// Add two vectors, the first being a Vector3i and the second a Vector3
Vector3 add(Vector3i v1, Vector3 v2)
{
    return Vector3(
        v1.x + v2.x,
        v1.y + v2.y,
        v1.z + v2.z,
    );
}

/// Subtract two vectors
Vector3 subtract(Vector3 v1, Vector3 v2)
{
    return Vector3(
        v1.x - v2.x,
        v1.y - v2.y,
        v1.z - v2.z,
    );
}

/// Multiply vector by scalar
Vector3 scale(Vector3 v, float scalar)
{
    return Vector3(
        v.x * scalar,
        v.y * scalar,
        v.z * scalar,
    );
}

/// Calculate two vectors cross product
Vector3 cross(Vector3 v1, Vector3 v2)
{
    return Vector3(
        v1.y*v2.z - v1.z*v2.y,
        v1.z*v2.x - v1.x*v2.z,
        v1.x*v2.y - v1.y*v2.x,
    );
}

/// Calculate square distance between two vectors
float distanceSquared(Vector3 v1, Vector3 v2)
{
    float result = 0.0f;

    float dx = v2.x - v1.x;
    float dy = v2.y - v1.y;
    float dz = v2.z - v1.z;
    result = dx*dx + dy*dy + dz*dz;

    return result;
}
