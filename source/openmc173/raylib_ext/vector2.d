module openmc173.raylib_ext.vector2;

import bindbc.raylib;


Vector2 add(Vector2 v1, Vector2 v2)
{
    return Vector2(
        v1.x + v2.x,
        v1.y + v2.y,
    );
}
