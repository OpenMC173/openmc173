module openmc173.raylib_ext.rectangle;

import bindbc.raylib;


Vector2 min(Rectangle rect)
{
    return Vector2(rect.x, rect.y);
}
