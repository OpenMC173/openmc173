module openmc173.app;

import bindbc.loader.sharedlib : ErrorInfo, errors;
import bindbc.raylib;
import openmc173.game.game_screen;
import openmc173.io.textures;
import openmc173.screen;
import std.algorithm.iteration;
import std.conv;
import std.stdio;
import std.string;


void main()
{
    enum SCREEN_WIDTH = 1280;
    enum SCREEN_HEIGHT = 720;

    initRaylib();

    // Initialization
    SetConfigFlags(ConfigFlags.FLAG_WINDOW_RESIZABLE | ConfigFlags.FLAG_VSYNC_HINT);
    SetTraceLogLevel(TraceLogLevel.LOG_WARNING);
    InitWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "OpenMC173");
    scope(exit) CloseWindow();

    Textures textures = new Textures("Default 16x16");
    scope(exit) destroy(textures);

    Screen screen = new GameScreen(textures);
    scope(exit) destroy(screen);

    // Main game loop
    while (!WindowShouldClose())
    {
        screen.onRenderLoop(GetFrameTime());
    }
}


/// Throws exception if loading fails.
public void initRaylib()
{
    RaylibSupport loadRaylibResult;

    version (linux)
    {
        loadRaylibResult = loadRaylib("./libraylib.so");
    }
    else version (OSX)
    {
        loadRaylibResult = loadRaylib("./libraylib.dylib");
    }
    else
    {
        static assert(false, "Building on this platform is not (yet) supported.");
    }

    if (loadRaylibResult != raylibSupport)
    {
        const(ErrorInfo)[] errors = errors();
        throw new Exception(
            "Error loading raylib: " ~ loadRaylibResult.to!string ~ "\n" ~
            errors
                .map!(errorInfo => errorInfo.error().to!string ~ ": " ~ errorInfo.message().to!string)
                .fold!((acc, e) => acc ~ e ~ '\n')("")
        );
    }

    writeln("Raylib version: ", loadRaylibResult);
    writeln("Raylib loaded:  ", loadedRaylibVersion);
}
