#!/usr/bin/env dub
/+ dub.sdl:
    name "test-watch"
    version "1.0.0"
    license "public domain"
    dependency "fswatch" version="~>0.5"
+/

import core.thread;
import fswatch;
import std.datetime;
import std.process;

void main(string[] args)
{
    FileWatch watcher = FileWatch("source/", true);

    runTests(args);

    while (true)
    {
        if (watcher.getEvents().length > 0) runTests(args);

        Thread.sleep(500.msecs);
    }
}

void runTests(string[] args)
{
    Pid clearPid = spawnShell("clear");
    wait(clearPid);
    Pid dubTestPid = spawnProcess(["dub", "test", "--compiler=dmd"] ~ args[1..$]);
    wait(dubTestPid);
}
